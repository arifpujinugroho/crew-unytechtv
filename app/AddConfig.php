<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddConfig extends Model
{
    //
    protected $table = 'addconfig';
    protected $primaryKey = 'id';
}
