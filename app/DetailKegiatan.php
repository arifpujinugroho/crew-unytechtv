<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailKegiatan extends Model
{
    //
    protected $table = 'detail_kegiatan';

    public function kepengurusan()
	{
		return $this->belongsTo('Kepengurusan', 'id_kepengurusan');
    }

    public function mahasiswa()
	{
		return $this->belongsTo('Mahasiswa', 'id_mahasiswa');
    }
}
