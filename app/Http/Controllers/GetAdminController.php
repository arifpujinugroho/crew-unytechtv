<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Hash;
use Auth;
use App\User;
use Redirect;
use App\Prodi;
use App\Kegiatan;
use App\AddConfig;
use App\Mahasiswa;
use App\Kepengurusan;
use App\DetailKegiatan;

class GetAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    //
    public function Index(){
        return view('auth.admin.home');
    }

    public function SearchCrew(Request $request){

        return view('auth.admin.search');
    }

    public function Search(Request $request){
        
        $users = Mahasiswa::when($request->keyword, function ($query) use ($request) {
            $query->where('namalengkap', 'like', "%{$request->keyword}%") // search by namalengkap
                ->orWhere('nim', 'like', "%{$request->keyword}%"); // or by nim
            })
              ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
              ->paginate($request->limit ? $request->limit : 5);
        
        $users->appends($request->only('keyword'));
        
        return view('auth.admin.value', compact('users'));
    }

    public function Kegiatan(Request $request){
        $tasks = Kegiatan::when($request->keyword, function ($query) use ($request) {
            $query->where('nama_kegiatan', 'like', "%{$request->keyword}%") // search by namakegiatan
                ->orWhere('tanggal_kegiatan', 'like', "%{$request->keyword}%") // or by tanggalkegiatan
                ->orWhere('nomer_surat', 'like', "%{$request->keyword}%"); // or by nomersurat
            })
              ->orderBy('created_at', 'DESC')
              ->paginate($request->limit ? $request->limit : 5);
        
        $tasks->appends($request->only('keyword'));

        return view('auth.admin.addtask', compact('tasks'));
    }

    public function AddTaskCrew($id, Request $request){
        $data = Kegiatan::whereid($id)->first();

        $is_available = (Kegiatan::whereid($id)->count() == 0) ? true : false ;
        if ($is_available) {
            return Redirect('/admin/kegiatan')->with('adminaddtask', 'Maaf, ID kegiatan tersebut tidak ditemukan');
        } else {

        $mhs = Mahasiswa::when($request->keyword, function ($query) use ($request) {
            $query->where('namalengkap', 'like', "%{$request->keyword}%") // search by 
                ->orWhere('namapanggilan', 'like', "%{$request->keyword}%") // or by 
                ->orWhere('nim', 'like', "%{$request->keyword}%"); // or by
            })
              ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
              ->paginate($request->limit ? $request->limit : 5);
        
        $mhs->appends($request->only('keyword'));

        $prodi = Prodi::all();

        $crew = DetailKegiatan::whereid_kegiatan($id)
            ->join('mahasiswa', 'mahasiswa.id', '=', 'detail_kegiatan.id_mahasiswa')
            ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
            ->get();
        $jmlcrew = DetailKegiatan::whereid_kegiatan($id)->count();
        return view('auth.admin.addtaskcrew', compact('data','mhs','crew','prodi','jmlcrew'));
        }
    }

    public function Print($id){
        $is_available = (Kegiatan::whereid($id)->count() == 0) ? true : false ;
        if ($is_available) {
            return Redirect('/admin/kegiatan')->with('adminaddtask', 'Maaf, ID kegiatan tersebut tidak ditemukan');
        } else {
            $is_kosong = (DetailKegiatan::whereid_kegiatan($id)->count() == 0) ? true : false ;
            
            if ($is_kosong) {
                return Redirect('/admin/kegiatan/'.$id)->with('adminaddtaskcrew', 'Maaf, Crew yang bertugas belum ada');
            } else {
                $data = Kegiatan::whereid($id)->first();

                $crew = DetailKegiatan::whereid_kegiatan($id)
                    ->join('mahasiswa', 'mahasiswa.id', '=', 'detail_kegiatan.id_mahasiswa')
                    ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
                    ->get();
                return view('auth.admin.printijin',compact('crew','data'));
            }
        }
    }

    public function Pengelola(Request $request){
        $mhs = Mahasiswa::when($request->keyword, function ($query) use ($request) {
            $query->where('namalengkap', 'like', "%{$request->keyword}%") // search by 
                ->orWhere('namapanggilan', 'like', "%{$request->keyword}%") // or by 
                ->orWhere('nim', 'like', "%{$request->keyword}%"); // or by
            })
              ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
              ->paginate($request->limit ? $request->limit : 5);
        
        $mhs->appends($request->only('keyword'));

        $crew = User::wherelevel('pengurus')->join('mahasiswa','mahasiswa.nim','=','users.username')
        ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
        ->get();
        $jmlcrew = User::wherelevel('pengurus')->count();

        return view('auth.admin.pengurus', compact('mhs','crew','jmlcrew'));
    }
}
