<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Hash;
use Auth;
use User;
use Redirect;
use App\Prodi;
use App\AddConfig;
use App\Mahasiswa;
use App\Kepengurusan;
use App\DetailPengurus;

class GetGuestController extends Controller
{
    //
    public function Index(){
        $tahun = Kepengurusan::whereaktif('1')->first()->tahun;
        $pcb = AddConfig::wherename('moderecruitment')->first()->isinya;
        $thnorgn = Kepengurusan::whereaktif('1')->first()->tahun;
        
        $tmhs = Mahasiswa::where('status_crew','!=','Calon')->count();
        $tcrew = Mahasiswa::wherestatus_crew('Penurus')
                ->orwhere('status_crew','=','Anggota')
                ->count();
        $talumni = Mahasiswa::wherestatus_crew('Alumni')->count();
        
        $tcalon = Mahasiswa::wherestatus_crew('Calon')
                ->where('tahunpcb','=',$thnorgn)->count();
        $tpengurus = DetailPengurus::select('*')
                ->join('mahasiswa', 'mahasiswa.id', '=', 'detail_pengurus.id_mahasiswa')
                ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
                ->join('kepengurusan', 'kepengurusan.id', '=', 'detail_pengurus.id_kepengurusan')
                ->where('kepengurusan.aktif', '=', '1')->count();
        return view('guest.home',compact('pcb','tahun','thnorgn','tmhs','tcrew','talumni','tcalon','tpengurus'));
    }
    
    public function Artisan(){
       Artisan::call('view:clear');
       Artisan::call('cache:clear');
       Artisan::call('route:clear');
       return redirect('/')->with('status','Views Cleared!');
    }
    
    
    public function Login(){
        $tahun = Kepengurusan::whereaktif('1')->first()->tahun;
        $pcb = AddConfig::wherename('moderecruitment')->first()->isinya;
        return view('guest.login',compact('pcb','tahun'));
    }

    public function Register(){
        $pcb = AddConfig::wherename('moderecruitment')->first()->isinya;
        $hotline = AddConfig::wherename('hotline')->first()->isinya;
        $tahun = Kepengurusan::whereaktif('1')->first()->tahun;
        $prodi = Prodi::all();

        return view('guest.register', compact('pcb','prodi','tahun','hotline'));
    }

    public function Custom($thn){
        $cekpcb = Kepengurusan::whereaktif('1')->first()->tahun;
        if ($cekpcb == $thn) {
            return Redirect('/pcb-unytechtv');
        } else {
            $is_available = (Kepengurusan::wheretahun($thn)->count() == 0) ? true : false ;
            if ($is_available) {
                return Redirect('/home')->with('status', 'Maaf, Penerimaan Crew pada Tahun '.$thn.' Tidak Tersedia!');
            } else {
                $pcb = AddConfig::wherename('moderecruitment')->first()->isinya;
                $hotline = AddConfig::wherename('hotline')->first()->isinya;
                $tahun = Kepengurusan::whereaktif('1')->first()->tahun;
                $prodi = Prodi::all();
    
                return view('guest.custom', compact('pcb','prodi','tahun','thn','hotline'));
            }
        }
        
    }

    public function Pengurus(){
        $tahun = Kepengurusan::whereaktif('1')->first()->tahun;
        $pcb = AddConfig::wherename('moderecruitment')->first()->isinya;
        $thnorgn = Kepengurusan::whereaktif('1')->first()->tahun;
        
        $data = DetailPengurus::select('*')
        ->join('mahasiswa', 'mahasiswa.id', '=', 'detail_pengurus.id_mahasiswa')
        ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
        ->join('kepengurusan', 'kepengurusan.id', '=', 'detail_pengurus.id_kepengurusan')
        ->where('kepengurusan.aktif', '=', '1')
        ->orderBy('prioritas', 'asc')
        ->get();

        $jml = DetailPengurus::select('*')
        ->join('mahasiswa', 'mahasiswa.id', '=', 'detail_pengurus.id_mahasiswa')
        ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
        ->join('kepengurusan', 'kepengurusan.id', '=', 'detail_pengurus.id_kepengurusan')
        ->where('kepengurusan.aktif', '=', '1')->count();
					
        return view('guest.pengurus', compact('pcb','thnorgn','tahun','data','jml'));
    }

    public function SearchCrew(Request $request){
        $tahun = Kepengurusan::whereaktif('1')->first()->tahun;
        $pcb = AddConfig::wherename('moderecruitment')->first()->isinya;

        return view('guest.search', compact('pcb','tahun'));
    }

    public function Search(Request $request){
        $tahun = Kepengurusan::whereaktif('1')->first()->tahun;
        $pcb = AddConfig::wherename('moderecruitment')->first()->isinya; 
        
        $users = Mahasiswa::when($request->keyword, function ($query) use ($request) {
            $query->where('namalengkap', 'like', "%{$request->keyword}%") // search by namalengkap
                ->orWhere('nim', 'like', "%{$request->keyword}%")
                ->orWhere('namapanggilan', 'like', "%{$request->keyword}%"); // or by nim
            })
              ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
              ->paginate($request->limit ? $request->limit : 5);
        
        $users->appends($request->only('keyword'));
        
        return view('guest.value', compact('pcb','tahun','users'));
    }

    public function Nim($nim){
        $tahun = Kepengurusan::whereaktif('1')->first()->tahun;
        $pcb = AddConfig::wherename('moderecruitment')->first()->isinya;

        $is_available = (Mahasiswa::wherenim($nim)->where('status_crew','!=','Calon')->count() == 0) ? true : false ;
        if ($is_available) {
            return Redirect('/home')->with('status', 'Maaf, Crew dengan '.$nim.' tidak ditemukan');
        } else {
            $idmhs = Mahasiswa::wherenim($nim)->first()->id;

            $data = Mahasiswa::wherenim($nim)
                    ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
                    ->first();
            $status = DetailPengurus::whereid_mahasiswa($idmhs)->count();

            $jabatan = DetailPengurus::whereid_mahasiswa($idmhs)
                     ->join('kepengurusan', 'kepengurusan.id', '=', 'detail_pengurus.id_kepengurusan')
                     ->where('kepengurusan.aktif', '=', '1')->first();

            return view('guest.nimcrew', compact('pcb','tahun','data','status','jabatan'));   
        }
    }

    public function Dpo2019(){
        $pcb = AddConfig::wherename('moderecruitment')->first()->isinya;
        $hotline = AddConfig::wherename('hotline')->first()->isinya;
        $tahun = Kepengurusan::whereaktif('1')->first()->tahun;
        $prodi = Prodi::all();
        return view('guest.fake.2016', compact('pcb','prodi','tahun','hotline'));
    }

    public function Pengurus2019(){
        $pcb = AddConfig::wherename('moderecruitment')->first()->isinya;
        $hotline = AddConfig::wherename('hotline')->first()->isinya;
        $tahun = Kepengurusan::whereaktif('1')->first()->tahun;
        $prodi = Prodi::all();
        return view('guest.fake.2017', compact('pcb','prodi','tahun','hotline'));
    }

    public function Crew2018(){
        $pcb = AddConfig::wherename('moderecruitment')->first()->isinya;
        $hotline = AddConfig::wherename('hotline')->first()->isinya;
        $tahun = Kepengurusan::whereaktif('1')->first()->tahun;
        $prodi = Prodi::all();
        return view('guest.fake.2018', compact('pcb','prodi','tahun','hotline'));
    }
}
