<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Hash;
use Auth;
use App\User;
use Redirect;
use App\Prodi;
use App\Struktur;
use App\Kegiatan;
use App\AddConfig;
use App\Mahasiswa;
use App\Kepengurusan;
use App\DetailPengurus;
use App\DetailKegiatan;

class GetMahasiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('mhs');
    }
    //
    public function Index(){
        $idmhs = Auth::user()->mahasiswa->id;
            $status = DetailPengurus::whereid_mahasiswa($idmhs)->count();

            $jabatan = DetailPengurus::whereid_mahasiswa($idmhs)
                     ->join('kepengurusan', 'kepengurusan.id', '=', 'detail_pengurus.id_kepengurusan')
                     ->where('kepengurusan.aktif', '=', '1')->first();
                     
        return view('auth.mahasiswa.home', compact('status','jabatan'));
    }

    public function Activity(Request $request){
        $id_mhs = Auth::user()->mahasiswa->id;

        $jml = DetailKegiatan::whereid_mahasiswa($id_mhs)->count();

        $tasks = DetailKegiatan::when($request->keyword, function ($query) use ($request) {
            $query->where('nama_kegiatan', 'like', "%{$request->keyword}%") // search by namakegiatan
                ->orWhere('tanggal_kegiatan', 'like', "%{$request->keyword}%") // or by tanggalkegiatan
                ->orWhere('jenis_kegiatan', 'like', "%{$request->keyword}%"); // or by nomersurat
            })
              ->join('kegiatan', 'kegiatan.id', '=', 'detail_kegiatan.id_kegiatan')
              ->where('id_mahasiswa', '=', $id_mhs)
              ->paginate($request->limit ? $request->limit : 5);
        
        $tasks->appends($request->only('keyword'));
        
        return view('auth.mahasiswa.activity', compact('jml','tasks'));
    }
}
