<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Hash;
use Auth;
use App\User;
use Redirect;
use App\Prodi;
use App\Struktur;
use App\Kegiatan;
use App\AddConfig;
use App\Mahasiswa;
use App\Kepengurusan;
use App\DetailPengurus;
use App\DetailKegiatan;

class GetPengurusController extends Controller
{
    public function __construct()
    {
        $this->middleware('pengurus');
    }
    //
    public function Index(){
        $tahun = Kepengurusan::whereaktif('1')->first()->tahun;
        $pcb = AddConfig::wherename('moderecruitment')->first()->isinya;
        $thnorgn = Kepengurusan::whereaktif('1')->first()->tahun;
        
        $tmhs = Mahasiswa::where('status_crew','!=','Calon')->count();
        $tcrew = Mahasiswa::wherestatus_crew('Penurus')
                ->orwhere('status_crew','=','Anggota')
                ->count();
        $talumni = Mahasiswa::wherestatus_crew('Alumni')->count();
        
        $tcalon = Mahasiswa::wherestatus_crew('Calon')
                ->where('tahunpcb','=',$thnorgn)->count();
        $tpengurus = DetailPengurus::select('*')
                ->join('mahasiswa', 'mahasiswa.id', '=', 'detail_pengurus.id_mahasiswa')
                ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
                ->join('kepengurusan', 'kepengurusan.id', '=', 'detail_pengurus.id_kepengurusan')
                ->where('kepengurusan.aktif', '=', '1')->count();


            $idmhs = Auth::user()->mahasiswa->id;

            $status = DetailPengurus::whereid_mahasiswa($idmhs)->count();

            $jabatan = DetailPengurus::whereid_mahasiswa($idmhs)
                     ->join('kepengurusan', 'kepengurusan.id', '=', 'detail_pengurus.id_kepengurusan')
                     ->where('kepengurusan.aktif', '=', '1')->first();
                     
        return view('auth.pengurus.home', compact('status','jabatan','thnorgn','pcb','tmhs','tcrew','talumni','tcalon','tpengurus'));
    }

    public function Activity(Request $request){
        $id_mhs = Auth::user()->mahasiswa->id;

        $jml = DetailKegiatan::whereid_mahasiswa($id_mhs)->count();

        $tasks = DetailKegiatan::when($request->keyword, function ($query) use ($request) {
            $query->where('nama_kegiatan', 'like', "%{$request->keyword}%") // search by namakegiatan
                ->orWhere('tanggal_kegiatan', 'like', "%{$request->keyword}%") // or by tanggalkegiatan
                ->orWhere('jenis_kegiatan', 'like', "%{$request->keyword}%"); // or by nomersurat
            })
              ->join('kegiatan', 'kegiatan.id', '=', 'detail_kegiatan.id_kegiatan')
              ->where('id_mahasiswa', '=', $id_mhs)
              ->paginate($request->limit ? $request->limit : 5);
        
        $tasks->appends($request->only('keyword'));
        
        return view('auth.pengurus.activity', compact('jml','tasks'));
    }
    public function Struktur(Request $request){
        $thnorgn = Kepengurusan::whereaktif('1')->first()->tahun;
        $data = DetailPengurus::select('*')
        ->join('mahasiswa', 'mahasiswa.id', '=', 'detail_pengurus.id_mahasiswa')
        ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
        ->join('kepengurusan', 'kepengurusan.id', '=', 'detail_pengurus.id_kepengurusan')
        ->where('kepengurusan.aktif', '=', '1')
        ->orderBy('prioritas', 'asc')
        ->get();

        $jml = DetailPengurus::select('*')
        ->join('mahasiswa', 'mahasiswa.id', '=', 'detail_pengurus.id_mahasiswa')
        ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
        ->join('kepengurusan', 'kepengurusan.id', '=', 'detail_pengurus.id_kepengurusan')
        ->where('kepengurusan.aktif', '=', '1')->count();

        $mhs = Mahasiswa::when($request->keyword, function ($query) use ($request) {
            $query->where('namalengkap', 'like', "%{$request->keyword}%") // search by 
                ->orWhere('namapanggilan', 'like', "%{$request->keyword}%") // or by 
                ->orWhere('nim', 'like', "%{$request->keyword}%"); // or by
            })
              ->where('status_crew','!=','Calon')
              ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
              ->paginate($request->limit ? $request->limit : 5);
        
        $mhs->appends($request->only('keyword'));

        $struktur = Struktur::all();
					
        return view('auth.pengurus.pengurus', compact('thnorgn','data','jml','mhs','prodi','struktur'));        
    }

    public function UserCrew(){
        $data = Kepengurusan::select('*')->orderBy('tahun','desc')->get();
        return view('auth.pengurus.usercrew', compact('data'));
    }

    public function KelolaWeb(Request $request){
        $nimme = Auth::user()->mahasiswa->nim;        
        $mhs = Mahasiswa::when($request->keyword, function ($query) use ($request) {
            $query->where('namalengkap', 'like', "%{$request->keyword}%") // search by 
                ->orWhere('namapanggilan', 'like', "%{$request->keyword}%") // or by 
                ->orWhere('nim', 'like', "%{$request->keyword}%"); // or by
            })
              ->where('status_crew','!=','Calon')
              ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
              ->paginate($request->limit ? $request->limit : 5);
        
        $mhs->appends($request->only('keyword'));

        $kelola = User::wherelevel('pengurus')->join('mahasiswa','mahasiswa.nim','=','users.username')
        ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
        ->where('username' , '!=' , $nimme)
        ->get();
        $jmlkelola = User::wherelevel('pengurus')->count();
        
        $preview = User::wherelevel('preview')->join('mahasiswa','mahasiswa.nim','=','users.username')
        ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
        ->get();
        $jmlpreview = User::wherelevel('preview')->count();

        return view('auth.pengurus.kelola', compact('mhs','kelola','jmlkelola','preview','jmlpreview'));
    }

    public function Kegiatan(Request $request){
        $tasks = Kegiatan::when($request->keyword, function ($query) use ($request) {
            $query->where('nama_kegiatan', 'like', "%{$request->keyword}%") // search by namakegiatan
                ->orWhere('tanggal_kegiatan', 'like', "%{$request->keyword}%") // or by tanggalkegiatan
                ->orWhere('nomer_surat', 'like', "%{$request->keyword}%"); // or by nomersurat
            })
              ->orderBy('created_at', 'DESC')
              ->paginate($request->limit ? $request->limit : 5);
        
        $tasks->appends($request->only('keyword'));

        return view('auth.pengurus.addtask', compact('tasks'));
    }

    public function AddTaskCrew($id, Request $request){
        $data = Kegiatan::whereid($id)->first();

        $is_available = (Kegiatan::whereid($id)->count() == 0) ? true : false ;
        if ($is_available) {
            return Redirect('/admin/kegiatan')->with('adminaddtask', 'Maaf, ID kegiatan tersebut tidak ditemukan');
        } else {

        $mhs = Mahasiswa::when($request->keyword, function ($query) use ($request) {
            $query->where('namalengkap', 'like', "%{$request->keyword}%") // search by 
                ->orWhere('namapanggilan', 'like', "%{$request->keyword}%") // or by 
                ->orWhere('nim', 'like', "%{$request->keyword}%"); // or by
            })
              ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
              ->paginate($request->limit ? $request->limit : 5);
        
        $mhs->appends($request->only('keyword'));

        $prodi = Prodi::all();

        $crew = DetailKegiatan::whereid_kegiatan($id)
            ->join('mahasiswa', 'mahasiswa.id', '=', 'detail_kegiatan.id_mahasiswa')
            ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
            ->get();
        $jmlcrew = DetailKegiatan::whereid_kegiatan($id)->count();
        return view('auth.pengurus.addtaskcrew', compact('data','mhs','crew','prodi','jmlcrew'));
        }
    }

    public function CalonCrew(Request $request){
        $thnorgn = Kepengurusan::whereaktif('1')->first()->tahun;
        $modepcb = AddConfig::wherename('moderecruitment')->first()->isinya;
        $jml = Mahasiswa::wheretahunpcb($thnorgn)
        ->where('status_crew','=','Calon')->count();

        $mhs = Mahasiswa::when($request->keyword, function ($query) use ($request) {
            $query->where('namalengkap', 'like', "%{$request->keyword}%") // search by 
                ->orWhere('namapanggilan', 'like', "%{$request->keyword}%") // or by 
                ->orWhere('nim', 'like', "%{$request->keyword}%"); // or by
            })
              ->where('status_crew','=','Calon')
              ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
              ->where('tahunpcb', '=', $thnorgn)
              ->paginate($request->limit ? $request->limit : 5);
        
        $mhs->appends($request->only('keyword'));
					
        return view('auth.pengurus.calon', compact('thnorgn','jml','mhs','modepcb'));        
    }

    public function PCBUP(){
	    $sistemdown = AddConfig::wherename('moderecruitment')->first();
	    $sistemdown->isinya = "true";
	    $sistemdown->save();
    }

    public function PCBDOWN(){
        $sistemdown = AddConfig::wherename('moderecruitment')->first();
        $sistemdown->isinya = "false";
        $sistemdown->save(); 
    }
    public function Setting(){
        $tahun_kepengurusan = Kepengurusan::select('*')->orderBY('tahun','desc')->get();
        return view('auth.pengurus.setting', compact('tahun_kepengurusan'));   
    }

    public function User(Request $request){
        $jml = Mahasiswa::select('*')
        ->join('users','users.id', '=','mahasiswa.id_user')->count();
        $jmlnon = Mahasiswa::select('*')
        ->leftjoin('users','users.id', '=','mahasiswa.id_user')->count();

        $mhs = Mahasiswa::when($request->keyword, function ($query) use ($request) {
            $query->where('namalengkap', 'like', "%{$request->keyword}%") // search by 
                ->orWhere('namapanggilan', 'like', "%{$request->keyword}%") // or by 
                ->orWhere('nim', 'like', "%{$request->keyword}%"); // or by
            })
              ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
              ->join('users','users.id', '=','mahasiswa.id_user')
              ->paginate($request->limit ? $request->limit : 5);
        
        $mhs->appends($request->only('keyword'));

        $nonmhs = Mahasiswa::when($request->nonkeyword, function ($query) use ($request) {
            $query->where('namalengkap', 'like', "%{$request->nonkeyword}%") // search by 
                ->orWhere('namapanggilan', 'like', "%{$request->nonkeyword}%") // or by 
                ->orWhere('nim', 'like', "%{$request->nonkeyword}%"); // or by
            })
              ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
              ->leftjoin('users','users.id', '=','mahasiswa.id_user')
              ->paginate($request->limit ? $request->limit : 5);
        
        $nonmhs->appends($request->only('nonkeyword'));
					
        return view('auth.pengurus.user', compact('jml','jmlnon','mhs'));
    }
}
