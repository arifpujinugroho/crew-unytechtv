<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class GetPreviewController extends Controller
{
    public function __construct()
    {
        $this->middleware('preview');
    }
    //
    public function Index(){
        return view('auth.preview.home');
    }
}
