<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Validation\Validator;
use Hash;
use File;
use Auth;
use App\User;
use Redirect;
use App\Prodi;
use App\Struktur;
use App\Kegiatan;
use App\AddConfig;
use App\Mahasiswa;
use App\Kepengurusan;
use App\DetailPengurus;
use App\DetailKegiatan;

class PostAdminController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function GantiPass(Request $request){
        $pass = $request->get('password');
        $isme = Auth::user()->mahasiswa->nim;
        $cek = User::whereusername($isme)->first()->crypt_token;
        $dcrypt = Crypt::decryptString($cek);

        if($pass == $dcrypt){
            return redirect()->back()->with('password','old');
        }else{
            $ganti = User::whereusername($isme)->first();
            $ganti->password = Hash::make($pass);
            $ganti->crypt_token = Crypt::encryptString($pass);
            $ganti->save();
            return redirect()->back()->with('password','success');
        }
    }
    public function Search(Request $request){
        
        $users = Mahasiswa::when($request->keyword, function ($query) use ($request) {
            $query->where('namalengkap', 'like', "%{$request->keyword}%") // search by namalengkap
                ->orWhere('nim', 'like', "%{$request->keyword}%"); // or by nim
            })
              ->where('status_crew', '!=', 'Calon')
              ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
              ->paginate($request->limit ? $request->limit : 5);
        
        $users->appends($request->only('keyword'));
        
        return view('auth.admin.value', compact('users'));
    }

    public function AddKegiatan(Request $request){
        
        $kgt = new Kegiatan();
        $kgt->jenis_kegiatan = $request->get('jns_kgt');
        $kgt->nama_kegiatan = $request->get('nametask');
        $kgt->tanggal_kegiatan = $request->get('tgl-task');
        $kgt->nomer_surat = $request->get('nomail');
        $kgt->tanggal_Surat = $request->get('tgl-mail');
        $kgt->penanggungjawab = $request->get('pj');
        $kgt->nama_penanggungjawab = $request->get('nama-pj');
        $kgt->nip = $request->get('nip-pj');
        $kgt->created_by = "Admin";
        $kgt->save();

        return redirect()->back()->with('adminaddtask','Success');
    }

    public function AddCrew(Request $request){
        
        $is_kosong = (Mahasiswa::wherenim($request->get('nim'))->count() == 0) ? true : false ;

        if($is_kosong){
            return redirect()->back()->with('adminaddtask','error');
        } else {
            $nimnya = $request->get('nim');
            $idmhs = Mahasiswa::wherenim($nimnya)->first()->id;
            $idku = $request->get('idkegiatan');
            $is_ada = (DetailKegiatan::whereid_kegiatan($idku)->whereid_mahasiswa($idmhs)->count() > 0) ? true : false ;
            if($is_ada){
                return redirect()->back()->with('adminaddtask','ada');
            } else {

            $mhs = Mahasiswa::wherenim($request->get('nim'))->first()->id;
            $dk = new DetailKegiatan();
            $dk->id_kegiatan = $request->get('idkegiatan');
            $dk->id_mahasiswa = $mhs;
            $dk->created_by = "Admin";
            $dk->save();  
            
            return redirect()->back()->with('adminaddtask','success');
            }
        }
    }

    public function AddCrewFt(Request $request){
        $nim = $request->get('nim');
        
        // cek user sudah ada atau belum
		$is_available = (User::whereusername($nim)->count() == 0) ? true : false ;

		if ($is_available) {

			//cek apakah data mahasiswa sudah ada atau belum
			$is_data_available = (Mahasiswa::wherenim($nim)->count() > 0) ? true : false ;

            // jika belum ada buat user dan identitas baru, jika sudah adabuat user dan update data mahasiswa
            if (!$is_data_available) {
                $namecall = "Created by Admin";
                $tahun = Kepengurusan::whereaktif('1')->first()->tahun;
                
                $user = new User();
                $user->username = $nim;
                $user->password = Hash::make($nim);
                $user->save();

                $mhs = new Mahasiswa();
                $mhs->nim = $nim;
                $mhs->id_user = $user->id;
                $mhs->tahunpcb = $tahun;
                $mhs->namapanggilan = $namecall;
                $mhs->namalengkap = $request->get('nama_lengkap');
                $mhs->alamat = $namecall;
                $mhs->id_prodi = $request->get('prodi');
                $mhs->jenis_kel = $request->get('jns_kel');
                $mhs->agama = $namecall;
                $mhs->tempatlahir = $namecall;
                $mhs->tanggallahir = $namecall;
                $mhs->kontak = $namecall;
                $mhs->instagram = $namecall;
                $mhs->skill = $namecall;
                $mhs->pengalaman = $namecall;
                $mhs->outorganitation = $namecall;
                $mhs->posisi = $namecall;
                $mhs->alasan = $namecall;
                $mhs->kontribusi = $namecall;
                $mhs->fotoawal = "";
                $mhs->save();

                $uid = $mhs->id_user;
                $deluser = User::find($uid);
                $uid->delete();

                return redirect()->back()->with('adminaddtask','success');

            }else{
                return redirect()->back()->with('adminaddtask','avalible');
            }
        }else{
            return redirect()->back()->with('adminaddtask','avalible');
        }
    }

    public function AddCrewNonFt(Request $request){
        $nim = $request->get('nim');
        
        // cek user sudah ada atau belum
		$is_available = (User::whereusername($nim)->count() == 0) ? true : false ;

		if ($is_available) {

			//cek apakah data mahasiswa sudah ada atau belum
			$is_data_available = (Mahasiswa::wherenim($nim)->count() > 0) ? true : false ;

            // jika belum ada buat user dan identitas baru, jika sudah adabuat user dan update data mahasiswa
            if (!$is_data_available) {
                $namecall = "Created by Admin";
                $tahun = Kepengurusan::whereaktif('1')->first()->tahun;
                
                $user = new User();
                $user->username = $nim;
                $user->password = Hash::make($nim);
                $user->save();

                $prodi = new Prodi();
                $prodi->nama_prodi = $request->get('prodi');
                $prodi->fak = $request->get('fakultas');
                $prodi->save();

                $mhs = new Mahasiswa();
                $mhs->nim = $nim;
                $mhs->id_user = $user->id;
                $mhs->tahunpcb = $tahun;
                $mhs->namapanggilan = $namecall;
                $mhs->namalengkap = $request->get('nama_lengkap');
                $mhs->alamat = $namecall;
                $mhs->id_prodi = $prodi->id;
                $mhs->jenis_kel = $request->get('jns_kel');
                $mhs->agama = $namecall;
                $mhs->tempatlahir = $namecall;
                $mhs->tanggallahir = $namecall;
                $mhs->kontak = $namecall;
                $mhs->instagram = $namecall;
                $mhs->skill = $namecall;
                $mhs->pengalaman = $namecall;
                $mhs->outorganitation = $namecall;
                $mhs->posisi = $namecall;
                $mhs->alasan = $namecall;
                $mhs->kontribusi = $namecall;
                $mhs->fotoawal = "";
                $mhs->save();

                $uid = $mhs->id_user;
                $deluser = User::find($uid);
                $uid->delete();

                return redirect()->back()->with('adminaddtask','success');

            }else{
                return redirect()->back()->with('adminaddtask','avalible');
            }
        }else{
            return redirect()->back()->with('adminaddtask','avalible');
        }
    }


    public function DelCrew(Request $request){
        
        $nimnya = $request->get('nim');
        $idmhs = Mahasiswa::wherenim($nimnya)->first()->id;
        $idku = $request->get('idkegiatan'); //buat ngambil dari form
        $data = DetailKegiatan::whereid_kegiatan($idku)->whereid_mahasiswa($idmhs)->first(); // untuk mencari datanya
        $data->delete();
        
        return redirect()->back()->with('adminaddtask','hapus');
    }

    public function AddPengelola(Request $request){
            $nimnya = $request->get('nim');
            $is_ada = (User::whereusername($nimnya)->wherelevel('pengurus')->count() > 0) ? true : false ;
            if($is_ada){
                return redirect()->back()->with('adminaddtask','ada');
            } else {

            $user = User::whereusername($nimnya)->first();
            $user->level = "pengurus";
            $user->save();
            return redirect()->back()->with('adminaddtask','success');
            }
    }

    public function DelPengelola(Request $request){
        
        $nimnya = $request->get('nim');
        $data = user::whereusername($nimnya)->first(); // untuk mencari datanya
        $data->level = "mahasiswa";
        $data->save();
        
        return redirect()->back()->with('adminaddtask','hapus');
    }
}
