<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Validation\Validator;
use Hash;
use File;
use Auth;
use App\User;
use Redirect;
use App\Prodi;
use App\Struktur;
use App\Kegiatan;
use App\AddConfig;
use App\Mahasiswa;
use App\Kepengurusan;
use App\DetailPengurus;
use App\DetailKegiatan;

class PostGuestController extends Controller
{
    //
    public function Daftar(Request $request){
        $pcb = AddConfig::wherename('moderecruitment')->first()->isinya;
        if($pcb == 'true'){

            $nim = $request->get('nim');
            // cek user sudah ada atau belum
		    $is_available = (User::whereusername($nim)->count() == 0) ? true : false ;

		    if ($is_available) {

		    	//cek apakah data mahasiswa sudah ada atau belum
		    	$is_data_available = (Mahasiswa::wherenim($nim)->count() > 0) ? true : false ;

                // jika belum ada buat user dan identitas baru, jika sudah adabuat user dan update data mahasiswa
                if (!$is_data_available) {
                    $file = $request->file('foto');
                    $namecall = $request->get('nama_panggilan');
                    $extensi = $file->getClientOriginalExtension();
                    $tahun = Kepengurusan::whereaktif('1')->first()->tahun;
                    $pin = mt_rand(9999, 99999);
                    $nama_file = $nim.'-'.$namecall.'-'.$pin.'.'.$extensi;
                    $destinasi = storage_path().'/foto/'.$tahun.'/client';
                    if (!File::isDirectory($destinasi)) {
                        File::makeDirectory($destinasi, 0775, true);
                    }
                    $user = new User();
                    $user->username = $nim;
                    $user->password = Hash::make($nim);
                    $user->crypt_token = Crypt::encryptString($nim);
                    $user->save();

                    $mhs = new Mahasiswa();
                    $mhs->nim = $nim;
                    $mhs->id_user = $user->id;
                    $mhs->tahunpcb = $tahun;
                    $mhs->namapanggilan = $namecall;
                    $mhs->namalengkap = $request->get('nama_lengkap');
                    $mhs->alamat = $request->get('alamat');
                    $mhs->id_prodi = $request->get('prodi');
                    $mhs->jenis_kel = $request->get('jns_kel');
                    $mhs->agama = $request->get('agama');
                    $mhs->tempatlahir = $request->get('tmp_lahir');
                    $mhs->tanggallahir = $request->get('tgl_lahir');
                    $mhs->kontak = $request->get('telepon');
                    $mhs->instagram = $request->get('instagram');
                    $mhs->skill = $request->get('skill');
                    $mhs->pengalaman = $request->get('pengalaman');
                    $mhs->outorganitation = $request->get('outorganisasi');
                    $mhs->posisi = $request->get('posisi');
                    $mhs->alasan = $request->get('alasan');
                    $mhs->kontribusi = $request->get('kontribusi');
                    $mhs->fotoawal = $nama_file;
                    $mhs->save();
                    
                    $file->move($destinasi, $nama_file);

                    Auth::loginUsingId($mhs->id_user);
                    return Redirect('/mhs')->with('register', 'success');
                }else{
                    $nimnya = $request->get('nim');
                    $cek = Mahasiswa::wherenim($nimnya)->first()->fotoawal;
                    $tahunfoto = Mahasiswa::wherenim($nimnya)->first()->tahunpcb;
                    $destinasifoto = 'storage/foto/'.$tahunfoto.'/client/'.$cek;
                    File::delete($destinasifoto);

                    $file = $request->file('foto');
                    $namepang = $request->get('nama_panggilan');
                    $extensi = $file->getClientOriginalExtension();
                    $tahun = Kepengurusan::whereaktif('1')->first()->tahun;
                    $pin = mt_rand(9999, 99999);
                    $nama_file = $nim.'-'.$namepang.'-'.$pin.'.'.$extensi;
                    $destinasi = storage_path().'/foto/'.$tahun.'/client';
                    
                    if (!File::isDirectory($destinasi)) {
                        File::makeDirectory($destinasi, 0775, true);
                    }

                    $id_user = Mahasiswa::wherenim($nim)->first()->id_user;
                    $user = new User();
                    $user->id = $id_user;
                    $user->username = $nim;
                    $user->password = Hash::make($nim);
                    $user->crypt_token = Crypt::encryptString($nim);
                    $user->save();

                    $mhs = Mahasiswa::wherenim($nim)->first();
                    $mhs->namapanggilan = $namepang;
                    $mhs->namalengkap = $request->get('nama_lengkap');
                    $mhs->alamat = $request->get('alamat');
                    $mhs->id_prodi = $request->get('prodi');
                    $mhs->jenis_kel = $request->get('jns_kel');
                    $mhs->agama = $request->get('agama');
                    $mhs->tempatlahir = $request->get('tmp_lahir');
                    $mhs->tanggallahir = $request->get('tgl_lahir');
                    $mhs->kontak = $request->get('telepon');
                    $mhs->instagram = $request->get('instagram');
                    $mhs->skill = $request->get('skill');
                    $mhs->pengalaman = $request->get('pengalaman');
                    $mhs->outorganitation = $request->get('outorganisasi');
                    $mhs->posisi = $request->get('posisi');
                    $mhs->alasan = $request->get('alasan');
                    $mhs->kontribusi = $request->get('kontribusi');
                    $mhs->fotoawal = $nama_file;
                    $mhs->save();

                    $file->move($destinasi, $nama_file);
                    
                    Auth::loginUsingId($id_user);

                    return Redirect('/mhs')->with('register', 'updated');
                }
            }else{
                return Redirect('/pcb-unytechtv')->with('register', 'registered');
            }
        } else {
            return Redirect('/pcb-unytechtv');
        }
    }

    public function Search(Request $request){
        $tahun = Kepengurusan::whereaktif('1')->first()->tahun;
        $pcb = AddConfig::wherename('moderecruitment')->first()->isinya; 
        
        $users = Mahasiswa::when($request->keyword, function ($query) use ($request) {
            $query->where('namalengkap', 'like', "%{$request->keyword}%") // search by namalengkap
                ->orWhere('nim', 'like', "%{$request->keyword}%")
                ->orWhere('namapanggilan', 'like', "%{$request->keyword}%"); // or by nim
            })
              ->where('status_crew', '!=', 'Calon')
              ->join('prodi', 'prodi.id', '=', 'mahasiswa.id_prodi')
              ->paginate($request->limit ? $request->limit : 5);
        
        $users->appends($request->only('keyword'));
        
        return view('guest.value', compact('pcb','tahun','users'));
    }


    public function Login(Request $request){

        if(Auth::attempt(['username' => $request->get('username'), 'password' => $request->get('password')]))
        {
            $user = User::find(Auth::id());
            //cek password harus enskripsi lagi atau tidak
            if(Hash::needsRehash($user->password)){
                $password = Hash::make($request->get('password'));
                $user->password = $password;
                $user->save();
            }
            if($user->level == 'admin'){
                return Redirect('/admin/home');
            } elseif ($user->level == 'pengurus'){
                return Redirect('/pengurus/home');
            } elseif ($user->level == 'mahasiswa'){
                return Redirect('/mhs/home');
            } elseif ($user->level == 'preview') {
                return Redirect('/preview/home');
            } else {
                Auth::logout();
                return Redirect::to('/login')
                ->with('login', 'denied');
            }
        } else {
            return Redirect::to('/login')->with('login', 'error');
        }
    }

    public function Custom($thn, Request $request){
        $nim = $request->get('nim');
        // cek user sudah ada atau belum
		$is_available = (User::whereusername($nim)->count() == 0) ? true : false ;

		if ($is_available) {

			//cek apakah data mahasiswa sudah ada atau belum
			$is_data_available = (Mahasiswa::wherenim($nim)->count() > 0) ? true : false ;

            // jika belum ada buat user dan identitas baru, jika sudah adabuat user dan update data mahasiswa
            if (!$is_data_available) {
                $file = $request->file('foto');
                $namecall = $request->get('nama_panggilan');
                $extensi = $file->getClientOriginalExtension();
                $tahun = $thn;
                $pin = mt_rand(9999, 99999);
                $nama_file = $nim.'-'.$namecall.'-'.$pin.'.'.$extensi;
                $destinasi = storage_path().'/foto/'.$tahun.'/client';
                if (!File::isDirectory($destinasi)) {
                    File::makeDirectory($destinasi, 0775, true);
                }
                $user = new User();
                $user->username = $nim;
                $user->password = Hash::make($nim);
                $user->crypt_token = Crypt::encryptString($nim);
                $user->save();

                $mhs = new Mahasiswa();
                $mhs->nim = $nim;
                $mhs->id_user = $user->id;
                $mhs->tahunpcb = $tahun;
                $mhs->namapanggilan = $namecall;
                $mhs->namalengkap = $request->get('nama_lengkap');
                $mhs->alamat = $request->get('alamat');
                $mhs->id_prodi = $request->get('prodi');
                $mhs->jenis_kel = $request->get('jns_kel');
                $mhs->agama = $request->get('agama');
                $mhs->tempatlahir = $request->get('tmp_lahir');
                $mhs->tanggallahir = $request->get('tgl_lahir');
                $mhs->kontak = $request->get('telepon');
                $mhs->instagram = $request->get('instagram');
                $mhs->fotoawal = $nama_file;
                $mhs->save();
                
                $file->move($destinasi, $nama_file);

                Auth::loginUsingId($mhs->id_user);
                return Redirect('/mhs')->with('register', 'success');
            }else{
                $nimnya = $request->get('nim');
                $cek = Mahasiswa::wherenim($nimnya)->first()->fotoawal;
                $tahunfoto = Mahasiswa::wherenim($nimnya)->first()->tahunpcb;
                $destinasifoto = 'storage/foto/'.$tahunfoto.'/client/'.$cek;
                File::delete($destinasifoto);

                $file = $request->file('foto');
                $namepang = $request->get('nama_panggilan');
                $extensi = $file->getClientOriginalExtension();
                $tahun = $thn;
                $pin = mt_rand(9999, 99999);
                $nama_file = $nim.'-'.$namepang.'-'.$pin.'.'.$extensi;
                $destinasi = storage_path().'/foto/'.$tahun.'/client';
                
                if (!File::isDirectory($destinasi)) {
                    File::makeDirectory($destinasi, 0775, true);
                }

                $id_user = Mahasiswa::wherenim($nim)->first()->id_user;
                $user = new User();
                $user->id = $id_user;
                $user->username = $nim;
                $user->password = Hash::make($nim);
                $user->crypt_token = Crypt::encryptString($nim);
                $user->save();

                $mhs = Mahasiswa::wherenim($nim)->first();
                $mhs->namapanggilan = $namepang;
                $mhs->tahunpcb = $tahun;
                $mhs->namalengkap = $request->get('nama_lengkap');
                $mhs->alamat = $request->get('alamat');
                $mhs->id_prodi = $request->get('prodi');
                $mhs->jenis_kel = $request->get('jns_kel');
                $mhs->agama = $request->get('agama');
                $mhs->tempatlahir = $request->get('tmp_lahir');
                $mhs->tanggallahir = $request->get('tgl_lahir');
                $mhs->kontak = $request->get('telepon');
                $mhs->instagram = $request->get('instagram');
                $mhs->fotoawal = $nama_file;
                $mhs->save();

                $file->move($destinasi, $nama_file);
                
                Auth::loginUsingId($id_user);

                return Redirect('/mhs')->with('register', 'updated');
            }
        }else{
            return Redirect('/pcb-unytechtv')->with('register', 'registered');
        }
    }




    //fake controller
    public function Dpo2019(Request $request){
        $nim = $request->get('nim');
        
        // cek user sudah ada atau belum
		$is_available = (User::whereusername($nim)->count() == 0) ? true : false ;

		if ($is_available) {

			//cek apakah data mahasiswa sudah ada atau belum
			$is_data_available = (Mahasiswa::wherenim($nim)->count() > 0) ? true : false ;

            // jika belum ada buat user dan identitas baru, jika sudah adabuat user dan update data mahasiswa
            if (!$is_data_available) {
                $file = $request->file('foto');
                $namecall = $request->get('nama_panggilan');
                $extensi = $file->getClientOriginalExtension();
                $tahun = "2016";
                $pin = mt_rand(9999, 99999);
                $nama_file = $nim.'-'.$namecall.'-'.$pin.'.'.$extensi;
                $destinasi = storage_path().'/foto/'.$tahun.'/client';
                if (!File::isDirectory($destinasi)) {
                    File::makeDirectory($destinasi, 0775, true);
                }
                $file->move($destinasi, $nama_file);
                $user = new User();
                $user->username = $nim;
                $user->password = Hash::make($nim);
                $user->save();

                $mhs = new Mahasiswa();
                $mhs->nim = $nim;
                $mhs->id_user = $user->id;
                $mhs->tahunpcb = $tahun;
                $mhs->namapanggilan = $namecall;
                $mhs->status_crew = "Alumni";
                $mhs->namalengkap = $request->get('nama_lengkap');
                $mhs->alamat = $request->get('alamat');
                $mhs->id_prodi = $request->get('prodi');
                $mhs->jenis_kel = $request->get('jns_kel');
                $mhs->agama = $request->get('agama');
                $mhs->tempatlahir = $request->get('tmp_lahir');
                $mhs->tanggallahir = $request->get('tgl_lahir');
                $mhs->kontak = $request->get('telepon');
                $mhs->instagram = $request->get('instagram');
                $mhs->skill = $request->get('skill');
                $mhs->pengalaman = $request->get('pengalaman');
                $mhs->outorganitation = $request->get('outorganisasi');
                $mhs->posisi = $request->get('posisi');
                $mhs->alasan = $request->get('alasan');
                $mhs->kontribusi = $request->get('kontribusi');
                $mhs->fotoawal = $nama_file;
                $mhs->save();
                return Redirect('/dpo2019')->with('status', 'Berhasil Input');

            }else{
                $id_user = Mahasiswa::wherenim($nim)->first()->id_user;
                $user = new User();
                $user->id = $id_user;
                $user->username = $nim;
                $user->password = Hash::make($nim);
                $user->save();

                return Redirect('/dpo2019')->with('status', 'Profile updated!');
            }
        }else{
            return Redirect('/dpo2019')->with('status', 'registered');
        }
    }

    public function Pengurus2019(Request $request){
        $nim = $request->get('nim');
        
        // cek user sudah ada atau belum
		$is_available = (User::whereusername($nim)->count() == 0) ? true : false ;

		if ($is_available) {

			//cek apakah data mahasiswa sudah ada atau belum
			$is_data_available = (Mahasiswa::wherenim($nim)->count() > 0) ? true : false ;

            // jika belum ada buat user dan identitas baru, jika sudah adabuat user dan update data mahasiswa
            if (!$is_data_available) {
                $file = $request->file('foto');
                $namecall = $request->get('nama_panggilan');
                $extensi = $file->getClientOriginalExtension();
                $tahun = "2017";
                $pin = mt_rand(9999, 99999);
                $nama_file = $nim.'-'.$namecall.'-'.$pin.'.'.$extensi;
                $destinasi = storage_path().'/foto/'.$tahun.'/client';
                if (!File::isDirectory($destinasi)) {
                    File::makeDirectory($destinasi, 0775, true);
                }
                $file->move($destinasi, $nama_file);
                $user = new User();
                $user->username = $nim;
                $user->password = Hash::make($nim);
                $user->save();

                $mhs = new Mahasiswa();
                $mhs->nim = $nim;
                $mhs->id_user = $user->id;
                $mhs->tahunpcb = $tahun;
                $mhs->status_crew = "Anggota";
                $mhs->namapanggilan = $namecall;
                $mhs->namalengkap = $request->get('nama_lengkap');
                $mhs->alamat = $request->get('alamat');
                $mhs->id_prodi = $request->get('prodi');
                $mhs->jenis_kel = $request->get('jns_kel');
                $mhs->agama = $request->get('agama');
                $mhs->tempatlahir = $request->get('tmp_lahir');
                $mhs->tanggallahir = $request->get('tgl_lahir');
                $mhs->kontak = $request->get('telepon');
                $mhs->instagram = $request->get('instagram');
                $mhs->skill = $request->get('skill');
                $mhs->pengalaman = $request->get('pengalaman');
                $mhs->outorganitation = $request->get('outorganisasi');
                $mhs->posisi = $request->get('posisi');
                $mhs->alasan = $request->get('alasan');
                $mhs->kontribusi = $request->get('kontribusi');
                $mhs->fotoawal = $nama_file;
                $mhs->save();
                return Redirect('/pengurus2019')->with('status', 'Berhasil Input');

            }else{
                $id_user = Mahasiswa::wherenim($nim)->first()->id_user;
                $user = new User();
                $user->id = $id_user;
                $user->username = $nim;
                $user->password = Hash::make($nim);
                $user->save();

                return Redirect('/pengurus2019')->with('status', 'Profile updated!');
            }
        }else{
            return Redirect('/pengurus2019')->with('status', 'Maaf Data Sudah ada');
        }
    }

    public function Crew2018(Request $request){
        $nim = $request->get('nim');
        
        // cek user sudah ada atau belum
		$is_available = (User::whereusername($nim)->count() == 0) ? true : false ;

		if ($is_available) {

			//cek apakah data mahasiswa sudah ada atau belum
			$is_data_available = (Mahasiswa::wherenim($nim)->count() > 0) ? true : false ;

            // jika belum ada buat user dan identitas baru, jika sudah adabuat user dan update data mahasiswa
            if (!$is_data_available) {
                $file = $request->file('foto');
                $namecall = $request->get('nama_panggilan');
                $extensi = $file->getClientOriginalExtension();
                $tahun = "2018";
                $pin = mt_rand(9999, 99999);
                $nama_file = $nim.'-'.$namecall.'-'.$pin.'.'.$extensi;
                $destinasi = storage_path().'/foto/'.$tahun.'/client';
                if (!File::isDirectory($destinasi)) {
                    File::makeDirectory($destinasi, 0775, true);
                }
                $file->move($destinasi, $nama_file);
                $user = new User();
                $user->username = $nim;
                $user->password = Hash::make($nim);
                $user->save();

                $mhs = new Mahasiswa();
                $mhs->nim = $nim;
                $mhs->id_user = $user->id;
                $mhs->tahunpcb = $tahun;
                $mhs->namapanggilan = $namecall;
                $mhs->status_crew = "Anggota";
                $mhs->namalengkap = $request->get('nama_lengkap');
                $mhs->alamat = $request->get('alamat');
                $mhs->id_prodi = $request->get('prodi');
                $mhs->jenis_kel = $request->get('jns_kel');
                $mhs->agama = $request->get('agama');
                $mhs->tempatlahir = $request->get('tmp_lahir');
                $mhs->tanggallahir = $request->get('tgl_lahir');
                $mhs->kontak = $request->get('telepon');
                $mhs->instagram = $request->get('instagram');
                $mhs->skill = $request->get('skill');
                $mhs->pengalaman = $request->get('pengalaman');
                $mhs->outorganitation = $request->get('outorganisasi');
                $mhs->posisi = $request->get('posisi');
                $mhs->alasan = $request->get('alasan');
                $mhs->kontribusi = $request->get('kontribusi');
                $mhs->fotoawal = $nama_file;
                $mhs->save();
                
                return Redirect('/pengurus2019')->with('status', 'Berhasil Input');

            }else{
                $id_user = Mahasiswa::wherenim($nim)->first()->id_user;
                $user = new User();
                $user->id = $id_user;
                $user->username = $nim;
                $user->password = Hash::make($nim);
                $user->save();

                return Redirect('/crew2018')->with('status', 'Profile updated!');
            }
        }else{
            return Redirect('/crew2018')->with('status', 'Maaf Data Sudah ada');
        }
    }
}
