<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Validation\Validator;
use Hash;
use File;
use Auth;
use App\User;
use Redirect;
use App\Prodi;
use App\Struktur;
use App\Kegiatan;
use App\AddConfig;
use App\Mahasiswa;
use App\Kepengurusan;
use App\DetailPengurus;
use App\DetailKegiatan;

class PostMahasiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('mhs');
    }
    //
    public function GantiPass(Request $request){
        $pass = $request->get('password');
        $isme = Auth::user()->mahasiswa->nim;
        $cek = User::whereusername($isme)->first()->crypt_token;
        $dcrypt = Crypt::decryptString($cek);

        if($pass == $dcrypt){
            return redirect()->back()->with('password','old');
        }else{
            $ganti = User::whereusername($isme)->first();
            $ganti->password = Hash::make($pass);
            $ganti->crypt_token = Crypt::encryptString($pass);
            $ganti->save();
            return redirect()->back()->with('password','success');
        }
    }

    
    public function GantiFoto(Request $request){
        $nimnya = $request->get('nim');
            $cek = Mahasiswa::wherenim($nimnya)->first()->fotoawal;
            $tahun = Mahasiswa::wherenim($nimnya)->first()->tahunpcb;
            $destinasi = 'storage/foto/'.$tahun.'/client/'.$cek;
            $fileData = File::get($destinasi);
            Storage::cloud()->put('client-'.$cek, $fileData);
            File::delete($destinasi);

            $file = $request->file('foto');
            $namecall = Mahasiswa::wherenim($nimnya)->first()->namapanggilan;
            $extensi = $file->getClientOriginalExtension();
            $tahun = Mahasiswa::wherenim($nimnya)->first()->tahunpcb;
            $pin = mt_rand(9999, 99999);
            $nama_file = $nimnya.'-'.$namecall.'-'.$pin.'.'.$extensi;
            $destinasi = storage_path().'/foto/'.$tahun.'/client';
            if (!File::isDirectory($destinasi)) {
                File::makeDirectory($destinasi, 0775, true);
            }
            $file->move($destinasi, $nama_file);
            $mhs = Mahasiswa::wherenim($nimnya)->first();
            $mhs->fotoawal = $nama_file;
            $mhs->save();
            
            return redirect()->back()->with('pengurusresmi','ada');
    }


}
