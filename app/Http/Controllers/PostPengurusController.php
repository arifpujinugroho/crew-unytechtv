<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Validation\Validator;
use Hash;
use File;
use Auth;
use App\User;
use Redirect;
use App\Prodi;
use App\Struktur;
use App\Kegiatan;
use App\AddConfig;
use App\Mahasiswa;
use App\Kepengurusan;
use App\DetailPengurus;
use App\DetailKegiatan;


class PostPengurusController extends Controller
{
    public function __construct()
    {
        $this->middleware('pengurus');
    }
    //
    public function GantiPass(Request $request){
        $pass = $request->get('password');
        $isme = Auth::user()->mahasiswa->nim;
        $cek = User::whereusername($isme)->first()->crypt_token;
        $dcrypt = Crypt::decryptString($cek);

        if($pass == $dcrypt){
            return redirect()->back()->with('password','old');
        }else{
            $ganti = User::whereusername($isme)->first();
            $ganti->password = Hash::make($pass);
            $ganti->crypt_token = Crypt::encryptString($pass);
            $ganti->save();
            return redirect()->back()->with('password','success');
        }
    }

    public function TambahPengurus(Request $request){
        $nim = $request->get('nim');
        $thnorgn = Kepengurusan::whereaktif('1')->first()->id;
        $id_mhs = Mahasiswa::wherenim($nim)->first()->id;
        $struktur = Struktur::whereid($request->get('posisi'))->first();
        
        $is_available = (DetailPengurus::whereid_mahasiswa($id_mhs)->whereid_kepengurusan($thnorgn)->count() == 0) ? true : false ;
        if ($is_available) {
            $pengurus = new DetailPengurus();
            $pengurus->id_mahasiswa = $id_mhs;
            $pengurus->posisi_lengkap = $struktur->nama_panjang;
            $pengurus->posisi_pendek = $struktur->nama_pendek;
            $pengurus->id_kepengurusan = $thnorgn;
            $pengurus->prioritas = $struktur->urutan;
            $pengurus->save();

            $cek = Mahasiswa::wherenim($nim)->first()->status_crew;
            if($cek != "Alumni"){
                $mhs = Mahasiswa::find($id_mhs);
                $mhs->status_crew = "Pengurus";
                $mhs->save();
            }
    
            return redirect()->back()->with('tambahpengurus','success');
        } else {
            return redirect()->back()->with('tambahpengurus','ada');
        }
    }

    public function HapusPengurus(Request $request){
        $nim = $request->get('nim');
        $thnorgn = Kepengurusan::whereaktif('1')->first()->id;
        $id_mhs = Mahasiswa::wherenim($nim)->first()->id;

            $pengurus = DetailPengurus::whereid_mahasiswa($id_mhs)->whereid_kepengurusan($thnorgn)->first();
            $pengurus->delete();

            $mhs = Mahasiswa::find($id_mhs);
            $mhs->status_crew = "Anggota";
            $mhs->save();
    
            return redirect()->back()->with('hapuspengurus','success');
    }

    public function AddPengelola(Request $request){
        $nimnya = $request->get('nim');
        $is_ada = (User::whereusername($nimnya)->wherelevel('pengurus')->count() > 0) ? true : false ;
        if($is_ada){
            return redirect()->back()->with('adminaddtask','ada');
        }else{ 
            $is_pre = (User::whereusername($nimnya)->wherelevel('preview')->count() > 0) ? true : false ;
            if($is_pre){
                return redirect()->back()->with('penguruskelola','ada');
            }else{
                $jmlh = (User::whereusername($nimnya)->wherelevel('pengurus')->count() > 7) ? true : false ;
                if($jmlh){
                    return redirect()->back()->with('penguruskelola','over');
                } else {
                    $user = User::whereusername($nimnya)->first();
                    $user->level = "pengurus";
                    $user->save();
                    return redirect()->back()->with('penguruskelola','success');
                }
            }
        }
    }

    public function AddPreview(Request $request){
        $nimnya = $request->get('nim');
        $is_ada = (User::whereusername($nimnya)->wherelevel('preview')->count() > 0) ? true : false ;
        if($is_ada){
            return redirect()->back()->with('adminaddtask','ada');
        }else{ 
            $is_pre = (User::whereusername($nimnya)->wherelevel('pengurus')->count() > 0) ? true : false ;
            if($is_pre){
                return redirect()->back()->with('penguruskelola','pengurus');
            }else{
                $user = User::whereusername($nimnya)->first();
                $user->level = "preview";
                $user->save();
                return redirect()->back()->with('penguruskelola','success');
            }
        }
    }

    public function DelPengelola(Request $request){
        
        $nimnya = $request->get('nim');
        $data = user::whereusername($nimnya)->first(); // untuk mencari datanya
        $data->level = "mahasiswa";
        $data->save();
        
        return redirect()->back()->with('penguruskelola','hapus');
    }

    public function FotoResmi(Request $request){
        $nimnya = $request->get('nim');
        $msiswa = Mahasiswa::wherenim($nimnya)->first()->fotoresmi;

        if($msiswa == "" || $msiswa == "null" || $msiswa == "NULL"){
            $file = $request->file('foto');
            $validator = Validator::make(
                array('file' => Input::file('file')),
                array('file' => 'max:5000')
            );

            if($validator->fails()){
                return redirect()->back()->with('pengurusresmi', 'fail');
            }

            $namecall = Mahasiswa::wherenim($nimnya)->first()->namapanggilan;
            $extensi = $file->getClientOriginalExtension();
            $tahun = Mahasiswa::wherenim($nimnya)->first()->tahunpcb;
            $pin = mt_rand(9999, 99999);
            $nama_file = $nimnya.'-'.$namecall.'-'.$pin.'.'.$extensi;
            $destinasi = storage_path().'/foto/'.$tahun.'/resmi';
            if (!File::isDirectory($destinasi)) {
                File::makeDirectory($destinasi, 0775, true);
            }
            $file->move($destinasi, $nama_file);
            $mhs = Mahasiswa::wherenim($nimnya)->first();
            $mhs->fotoresmi = $nama_file;
            $mhs->save();

            return redirect()->back()->with('pengurusresmi','kosong');
        }else{
            $cek = Mahasiswa::wherenim($nimnya)->first()->fotoresmi;
            $tahun = Mahasiswa::wherenim($nimnya)->first()->tahunpcb;
            $destinasi = 'storage/foto/'.$tahun.'/resmi/'.$cek;
            $fileData = File::get($destinasi);
            Storage::cloud()->put('resmi-'.$cek, $fileData);
            File::delete($destinasi);

            $file = $request->file('foto');
            $namecall = Mahasiswa::wherenim($nimnya)->first()->namapanggilan;
            $extensi = $file->getClientOriginalExtension();
            $tahun = Mahasiswa::wherenim($nimnya)->first()->tahunpcb;
            $pin = mt_rand(9999, 99999);
            $nama_file = $nimnya.'-'.$namecall.'-'.$pin.'.'.$extensi;
            $destinasi = storage_path().'/foto/'.$tahun.'/resmi';
            if (!File::isDirectory($destinasi)) {
                File::makeDirectory($destinasi, 0775, true);
            }
            $file->move($destinasi, $nama_file);
            $mhs = Mahasiswa::wherenim($nimnya)->first();
            $mhs->fotoresmi = $nama_file;
            $mhs->save();
            
            return redirect()->back()->with('pengurusresmi','ada');
        }
    }

    public function GantiFoto(Request $request){
        $nimnya = $request->get('nim');
            $cek = Mahasiswa::wherenim($nimnya)->first()->fotoawal;
            $tahun = Mahasiswa::wherenim($nimnya)->first()->tahunpcb;
            $destinasi = 'storage/foto/'.$tahun.'/client/'.$cek;
            $fileData = File::get($destinasi);
            Storage::cloud()->put('client-'.$cek, $fileData);
            File::delete($destinasi);

            $file = $request->file('foto');
            $namecall = Mahasiswa::wherenim($nimnya)->first()->namapanggilan;
            $extensi = $file->getClientOriginalExtension();
            $tahun = Mahasiswa::wherenim($nimnya)->first()->tahunpcb;
            $pin = mt_rand(9999, 99999);
            $nama_file = $nimnya.'-'.$namecall.'-'.$pin.'.'.$extensi;
            $destinasi = storage_path().'/foto/'.$tahun.'/client';
            if (!File::isDirectory($destinasi)) {
                File::makeDirectory($destinasi, 0775, true);
            }
            $file->move($destinasi, $nama_file);
            $mhs = Mahasiswa::wherenim($nimnya)->first();
            $mhs->fotoawal = $nama_file;
            $mhs->save();
            
            return redirect()->back()->with('pengurusresmi','ada');
    }
    
    public function ResetPass(Request $request){
        $nim = $request->get('nim');
            $ganti = User::whereusername($nim)->first();
            $ganti->password = Hash::make($nim);
            $ganti->crypt_token = Crypt::encryptString($nim);
            $ganti->save();
            return redirect()->back()->with('password','success');
    }
}
