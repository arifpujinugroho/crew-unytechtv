<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;
use Auth;

class IsLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check()){
            return $next($request);
        } else{     
            Auth::logout();
            return Redirect::to('/login')
            ->with('login', 'notlogin');
        }
    }
}
