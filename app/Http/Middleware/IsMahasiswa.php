<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;
use Auth;

class IsMahasiswa
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && $request->user()->level == 'mahasiswa'){
            return $next($request);
        } else{     
            Auth::logout();
            return Redirect::to('/login')
            ->with('login', 'notmhs');
        }
    }
}
