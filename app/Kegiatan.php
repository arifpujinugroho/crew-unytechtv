<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{
    //
    protected $table = 'kegiatan';
    public function detailkegiatan()
	{
		return $this->hasMany('DetailKegiatan', 'id_kegiatan');
	}
}
