<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kepengurusan extends Model
{
    //
    protected $table = 'kepengurusan';

    public function detailpengurus()
	{
		return $this->hasMany('DetailPengurus', 'id_kepengurusan');
	}
}
