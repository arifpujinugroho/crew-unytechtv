<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    //
    protected $table = 'mahasiswa';

    public function prodi()
	{
		return $this->belongsTo('App\Prodi', 'id_prodi');
    }
    
    public function user()
	{
		return $this->belongsTo('User', 'id_user');
    }

    public function detailpengurus()
	{
		return $this->hasMany('DetailPengurus', 'id_mahasiswa');
  }
  
    public function detailkegiatan()
	{
		return $this->hasMany('DetailKegiatan', 'id_mahasiswa');
	}
}
