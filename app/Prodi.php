<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{
    //
    protected $table = 'prodi';

    public function mahasiswa()
	{
		return $this->hasMany('Mahasiswa', 'id_prodi');
    }
    /*
    Kode Fakultas

    FIP = 1
    FBS = 2
    FMIPA = 3
    FIS = 4
    FT = 5
    FIK = 6
    FE = 8
    */
}
