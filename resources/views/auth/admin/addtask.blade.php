@extends('layout.master')

@section('title')
Kegiatan
@endsection


@section('content')
@if (session('adminaddtask'))
    <div class="alert alert-danger">
        {{ session('adminaddtask') }}
    </div>
@endif
<!-- Page-header start -->
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                <div class="d-inline">
                    <h4>Kegiatan</h4>
                    <span>Semua kegiatan yang ada di UNYtechTV</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ url('/') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Kegiatan</a>
                    </li>
                </ul>   
            </div>
        </div>
    </div>
</div>
<!-- Page-header end -->

<!-- Page-body start -->
<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <!-- Basic Button table start -->
            <div class="card">
                <div class="card-header">
                    <h3>Kegiatan</h3>
                    <button class="btn btn-success btn-sm waves-effect md-trigger f-right" data-toggle="modal" data-target="#tambah-Modal">Tambah Kegiatan</button>
                    <hr>
                    <form action="{{ url()->current() }}">
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <input type="text" name="keyword" class="form-control" placeholder="Search..." value="{{ request('keyword') }}">
                            </div>
                            <div class="col">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    Search
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-block">
                    <div class="dt-responsive table-responsive">
                        <table class="table table-striped table-bordered nowrap">
                            <thead>
                                <th>Tanggal Kegiatan</th>
                                <th>Nama Kegiatan</th>
                                <th>No Surat</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @foreach ($tasks as $t)
                                    <tr>
                                        <td>{{ $t->tanggal_kegiatan }}</td>
                                        <td>{{ $t->nama_kegiatan }}</td>
                                        <td>{{ $t->nomer_surat }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <button class="btn btn-success btn-mini" data-toggle="tooltip" title="Lihat"><i class="icofont icofont-eye-alt"></i></button>
                                                <button class="btn btn-warning btn-mini edit-modal" data-idnya="{{ $t->id }}" data-jeniskegiatan="{{ $t->jenis_kegiatan }}" data-namakegiatan="{{ $t->nama_kegiatan }}" data-tanggalkegiatan="{{ $t->tanggal_kegiatan }}" data-nomersurat="{{ $t->nomer_surat }}" data-tanggalsurat="{{ $t->tanggal_surat }}" data-penanjawab="{{ $t->penanggungjawab }}" data-namapj="{{ $t->nama_penanggungjawab }}" data-nippj="{{ $t->nip }}" data-toggle="tooltip" title="Edit"><i class="icofont icofont-pencil-alt-1"></i></button>
                                             <a href="{{url('admin/kegiatan/'.$t->id)}}"><button class="btn btn-default btn-mini" data-toggle="tooltip" title="Crew/Print"><i class="icofont icofont-people"></i></button></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $tasks->links() }}
                    </div>
                </div>
            </div>
            <!-- Basic Button table end -->
        </div>
    </div>
</div>
<!-- Page-body start -->
@endsection

@section('end')
<div class="modal fade" id="tambah-Modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Kegiatan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form action="{{url('admin/addkegiatan')}}" method="post">
                @csrf                   
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label"><small> Jenis Kegiatan </small><strong><span class="text-danger">*</span></strong></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <select name="jns_kgt" id="jns_kgt" class="form-control" required>
                                <option value="">-- Jenis Kegiatan --</option>
                                <option value="Dokumentasi">Dokumentasi</option>
                                <option value="Pengambilan Video">Pengambilan Video</option>
                                <option value="Streaming">Streaming</option>
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label"><small> Nama Kegiatan </small><strong><span class="text-danger">*</span></strong></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <textarea name="nametask" id="nametask" class="form-control" maxlength="255" cols="20" rows="2" required></textarea>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label"><small> Tanggal Kegiatan </small><strong><span class="text-danger">*</span></strong></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <input type="text" name="tgl-task" class="form-control" placeholder="Hari, XX Januari 20XX" required>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label"><small> Nomer Surat </small></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <input type="text" name="nomail" id="nomail" class="form-control" placeholder="000/SI/LABTV/III/20XX">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label"><small> Tanggal Surat </small></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <input type="text" name="tgl-mail" class="form-control" placeholder="XX Januari 20XX">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label"><small>Penangung Jawab </small></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <input type="text" name="pj" id="pj" class="form-control" placeholder="Posisi Penangung Jawab">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label"><small>Nama PJ </small></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <input type="text" name="nama-pj" id="nama-pj" class="form-control" placeholder="Nama Penangung Jawab">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label"><small>NIP PJ </small></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <input type="text" name="nip-pj" id="nip-pj" class="form-control form-control-primary" placeholder="NIP. XXXXxxXX XXXXX X XX">
                        </div>
                    </div>
                </div>
                <br> 
            
            </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect waves-light ">Save Task</button>
                    <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="edit-Modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Kegiatan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <input type="hidden" name="id" id="id-kegiatan">
                @csrf                   
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label"><small> Jenis Kegiatan </small><strong><span class="text-danger">*</span></strong></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <select name="jns_kel" id="jns_kel" class="form-control" required>
                                <option value="">-- Jenis Kegiatan --</option>
                                <option value="dokumentasi">Dokumentasi</option>
                                <option value="pengambilan video dan foto">Pengambilan Video dan Foto</option>
                                <option value="streaming">Streaming</option>
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label"><small> Nama Kegiatan </small><strong><span class="text-danger">*</span></strong></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <textarea name="nametask" id="nama-kegiatan" class="form-control" maxlength="255" cols="20" rows="2" required></textarea>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label"><small> Tanggal Kegiatan </small><strong><span class="text-danger">*</span></strong></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <input type="text" name="tgl-task" id="tgl-task" class="form-control" placeholder="Hari, XX Januari 20XX" required>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label"><small> Nomer Surat </small></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <input type="text" name="nomail" id="nomer-surat" class="form-control" placeholder="000/SI/LABTV/III/20XX">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label"><small> Tanggal Surat </small></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <input type="text" name="tgl-mail" id="tgl-surat" class="form-control" placeholder="XX Januari 20XX">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label"><small>Penangung Jawab </small></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <input type="text" name="pj" id="penjab" class="form-control" placeholder="Posisi Penangung Jawab">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label"><small>Nama PJ </small></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <input type="text" name="nama-pj" id="nama-penjab" class="form-control" placeholder="Nama Penangung Jawab">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label"><small>NIP PJ </small></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <input type="text" name="nip-pj" id="nip-penjab" class="form-control" placeholder="NIP. XXXXxxXX XXXXX X XX">
                        </div>
                    </div>
                </div>
                <br> 
            
            </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect waves-light ">Save Task</button>
                    <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script>
$('.edit-modal').click(function() {
    $('#id-kegiatan').val($(this).data('idnya'));
    $('#jns_kel').val($(this).data('jeniskegiatan'));
    $('#nama-kegiatan').val($(this).data('namakegiatan'));
    $('#tgl-task').val($(this).data('tanggalkegiatan'));
    $('#nomer-surat').val($(this).data('nomersurat'));
    $('#tgl-surat').val($(this).data('tanggalsurat'));
    $('#penjab').val($(this).data('penanjawab'));
    $('#nama-penjab').val($(this).data('namapj'));
    $('#nip-penjab').val($(this).data('nippj'));
    $('#edit-Modal').modal('show');
});
</script>
@endsection
