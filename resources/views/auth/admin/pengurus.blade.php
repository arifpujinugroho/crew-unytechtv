@extends('layout.master')

@section('title')
Pengelola User
@endsection

@section('content')
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-speed-meter bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>Pengelola Web</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{url('/')}}"> <i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Pengelola Web</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->
    
    <!-- Page-body start -->
    <div class="page-body">
        <div class="row">
            <!-- Current Progress Start -->
            <div class="col-md-12">
                <div class="card card-current-prog">
                    <div class="card-header">
                        <div class="card-header-left">
                            <h5>Pengelola Web Crew Management System</h5>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>NIM</th>
                                    <th>Prodi</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                   <?php $n=1; ?>
                                    @foreach ($crew as $c)
                                        <tr>
                                            <td>{{$n}}</td>
                                            <td>{{$c->namalengkap}}</td>
                                            <td>{{$c->nim}}</td>
                                            <td>{{$c->nama_prodi}}</td>
                                            <td>
                                                <form action="{{ url('admin/delpengelola') }}" method="post">
                                                @csrf
                                                <input type="hidden" name="nim" value="{{$c->nim}}">
                                                <div class="btn-group">
                                                    <button type="submit" class="btn btn-danger btn-mini"><i class="icofont icofont-ui-delete"></i> Hapus</button>
                                                </div>
                                            </form>
                                            </td>
                                        </tr>
                                    <?php $n++; ?>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-header-left">
                            <h5>Daftar Mahasiswa</h5><br>
                        </div>
                        <form action="{{ url()->current() }}">
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <input type="text" name="keyword" class="form-control form-control-sm" placeholder="Search..." value="{{ request('keyword') }}">
                                    </div>
                                    <div class="col">
                                        <button type="submit" class="btn btn-primary btn-mini">Search</button>
                                    </div>
                                </div>
                            </form>
                    </div>
                    <div class="card-block widget-last-task">
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <th>Nama Panjang</th>
                                        <th>Nama Panggilan</th>
                                        <th>NIM</th>
                                        <th>Prodi</th>
                                        @if($jmlcrew < 6)
                                        <th>Action</th>
                                        @endif
                                    </thead>
                                    <tbody>
                                        @foreach ($mhs as $m)
                                            <tr>
                                                <td><small>{{$m->namalengkap}}</small></td>
                                                <td><small>{{$m->namapanggilan}}</small></td>
                                                <td><small>{{$m->nim}}</small></td>
                                                <td><small>{{$m->nama_prodi}}</small></td>
                                                @if($jmlcrew < 6)
                                                <td>
                                                    <form action="{{ url('admin/addpengelola') }}" method="post">
                                                        @csrf
                                                            <input type="hidden" name="nim" value="{{$m->nim}}">
                                                    <button type="submit" class="btn btn-mini btn-success" data-toggle="tooltip" title="tambah {{$m->namapanggilan}}"> + </button>
                                                    </form>
                                                </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $mhs->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Task card end -->
        </div>
        <!-- =========== widget 2  are work end =================   -->
    </div>
    <!-- Page-body end -->
@endsection