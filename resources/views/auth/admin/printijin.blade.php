<!DOCTYPE html>

<!--


	***** HAYO.. Mau Ngapain Buka Inpect Element? *****
   ******** JANGAN ISENG BUKA BUKA KODINGANNYA ***********

//=========================================================//
//         //\\       ||====    ||      ||==========       //
//        //  \\      ||  //    ||      ||                 //
//       //    \\     || //     ||      ||========         //
//      //======\\    || \\     ||      ||                 //
//     //        \\   ||  \\    ||      ||                 //
//    //          \\  ||   \\   ||      ||                 //
//                                                         //
//        Created By Arif Puji Nugroho (Indonesia)         //
//     Website    : https://arifpujin.com/                 //
//     GitHub     : https://Github.com/arifpujin           //
//     Facebook   : https://facebook.com/arifpujin         //
//     Instagram  : https://instagram.com/reallifeapn      //
//     Whatsapp   : +6285885994505                         //
//     Email      : arifpujinugroho@gmail.com              //
//=========================================================//

****Crew Management System for UKMF UNYtechTV FT UNY 2019****
****Pendamping Proyek Akhir Skripsi || Proyek Mandiri UNY****
         ***** 5 Maret 2019 - Thesis Finish *****





-->







<html><head lang="en">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <!-- Favicon -->
    <link rel="icon" href="https://unytechtv.com/wp-content/uploads/2018/12/cropped-favicon-32x32.png" sizes="32x32" />
    <link rel="icon" href="https://unytechtv.com/wp-content/uploads/2018/12/cropped-favicon-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="https://unytechtv.com/wp-content/uploads/2018/12/cropped-favicon-180x180.png" />
    <meta name="msapplication-TileImage" content="https://unytechtv.com/wp-content/uploads/2018/12/cropped-favicon-270x270.png" />

        
<title>{{$data->nama_kegiatan}}</title>
    </head>
    <body>
        <style type="text/css">
            table{
                font-family: Arial;
                font-size: 10px;
                width:750px;
            }
        </style>
        <div class="portlet box blue">
            <div class="portlet-body">
                <table border="0" style="font-size: 17px;font-family: Times">
                    <tbody>
                        <tr>
                            <td align="center" width="120px"><img src="{{url('assets/images/uny.png')}}" alt="" height="100px"></td>
                            <td><p style="text-align: center;"><span style="font-size: 18px;">KEMENTERIAN RISET, TEKNOLOGI DAN PENDIDIKAN TINGGI<br>
                                        UNIVERSITAS NEGERI YOGYAKARTA<br>
                                        <strong>FAKULTAS TEKNIK</strong></span><br>
                                    <span style="font-size: 17px">Alamat : Kampus Karangmalang, Yogyakarta, 55281 <br>
                                        Telp. (0274) 586168 psw. 276,289,292 (0274) 586734 Fax. (0274) 586734 <br>
                                        Website : http://ft.uny.ac.id e-mail : ft@uny.ac.id ; teknik@uny.ac.id</span></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <br>
                <table width="100%">
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td width="33%" style="font-size: 17px; text-align: center;font-family: Times">Yogyakata, {{$data->tanggal_surat}}</td>
                        </tr>
                    </tbody>
                </table>

                <table border="0" style="font-family: Times; font-size: 17px;">
                    <tbody>
                        <tr>
                            <td width="1%">&nbsp;</td>
                            <td width="10%">No</td>
                            <td width="5%" align="center"> : </td>
                            <td width="84%"> {{$data->nomer_surat}} </td>
                        </tr>
                        <tr>
                            <td width="1%">&nbsp;</td>
                            <td width="10%">Hal</td>
                            <td width="5%" align="center"> : </td>
                            <td width="84%"> Permohonan Ijin </td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <table border="0" style="font-family: Times; font-size: 17px;">
                    <tbody>
                        <tr>
                            <td width="1%">&nbsp;</td>
                            <td width="98%"style="font-family: Times; font-size: 17px;"><strong>Yth, Bpk/Ibu</strong></td>
                            <td width="1%"></td>
                        </tr>
                        <tr>
                            <td width="1%">&nbsp;</td>
                            <td width="98%"style="font-family: Times; font-size: 17px;"><strong>Dosen Pengampu Mata Kuliah</strong></td>
                            <td width="1%"></td>
                        </tr>
                        <tr>
                            <td width="1%">&nbsp;</td>
                            <td width="98%"style="font-family: Times; font-size: 17px;"></td>
                            <td width="1%"></td>
                        </tr>
                        <tr>
                            <td width="1%">&nbsp;</td>
                            <td width="98%"style="line-height: 1.6; font-family: Times; font-size: 17px;">
                                Dengan hormat,<br>
                                sehubungan dengan kegiatan LAB TV untuk {{$data->jenis_kegiatan}}, maka dengan ini kami sampaikan bahwa mahasiswa:
                            </td>
                            <td width="1%"></td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <table border="0" style="font-family: Times; font-size: 17px;">
                    <tbody>
                        <!--mahasiswa table-->
                        <tr>
                            <td width="1%">&nbsp;</td>
                            <td width="98%"style="font-family: Times; font-size: 17px;">
                                <table width="100%">
                                    <tbody>
                                        <td width="1%">&nbsp;</td>
                                        <td width="98%">
                                            <!--Start Tabel Mahasiswa-->   
                                            <table style="border: 0.5px solid black;border-spacing: 0;font-size: 15px;">
                                                <thead>
                                                    <th style="border: 0.5px solid black;" width="5%">No.</th>
                                                    <th style="border: 0.5px solid black;">Nama Mahasiswa</th>
                                                    <th style="border: 0.5px solid black;" >Nim</th>
                                                    <th style="border: 0.5px solid black;" >Program Studi</th>
                                                </thead>
                                                <tbody>
                                                    <?php $n=1; ?>
                                                    @foreach ($crew as $c)
                                                    <tr>
                                                        <td style="border: 0.5px ; text-align: center;border-left: 0.5px solid black;border-right: 0.5px solid black; border-bottom: 0.5px solid black;">{{ $n }}</td>
                                                        <td style="text-transform: capitalize; border: 0.5px ; border-left: 0.5px solid black;border-right: 0.5px solid black; border-bottom: 0.5px solid black; padding-left: 5px;">{{ $c->namalengkap }}</td>
                                                        <td style="border: 0.5px ; border-left: 0.5px solid black;border-right: 0.5px solid black; border-bottom: 0.5px solid black; padding-left: 5px;">{{ $c->nim }}</td>
                                                        <td style="border: 0.5px ; border-left: 0.5px solid black;border-right: 0.5px solid black; border-bottom: 0.5px solid black; padding-left: 5px;">{{ $c->nama_prodi }}</td>
                                                    </tr>
                                                    <?php $n++; ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <!--End Tabel Mahasiswa-->
                                        </td>
                                        <td width="1%">&nbsp;</td>
                                    </tbody>
                                </table>
                            </td>
                            <td width="1%"></td>
                        </tr>
                        <!--mahasiswa table end-->
                    </tbody>
                </table>
                <br>
                <table border="0" style="font-family: Times; font-size: 17px;">
                        <tbody>
                            <tr>
                                <td width="1%">&nbsp;</td>
                                <td width="98%"style="line-height: 1.6; text-align: justify ;font-family: Times; font-size: 17px;">
                                        tidak dapat mengikuti kuliah pada hari {{ $data->tanggal_kegiatan }} dikarenakan melaksanakan tugas untuk 
                                        @if ($data->jenis_kegiatan)
                                             mendokumentasikan 
                                        @else
                                             menyiarkan secara langsung 
                                        @endif kegiatan {{ $data->nama_kegiatan }}. Dengan demikian, mohon Bapak/Ibu berkenan memberikan ijin kepada mahasiswa tersebut.
                                        <br>
                                        Demikian permohonan ini kami sampaikan, atas perhatian dan kerjasama Bapak/Ibu, kami ucapkan terima kasih.
                                </td>
                                <td width="1%">&nbsp;</td>
                            </tr>
                        </tbody>
                </table>
                <br><br>
                <table style="font-family: Times; font-size: 17px;">
                    <tbody>
                        <tr>
                            <td width="70%">&nbsp;</td>
                            <td width="30%">{{ $data->penanggungjawab}}</td>
                        </tr>
                        <tr>
                            <td width="70%" height="50px">&nbsp;</td>
                            <td width="30%" height="50px">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="70%">&nbsp;</td>
                            <td width="30%">{{ $data->nama_penanggungjawab }}<br>{{ $data->nip}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
</body></html>