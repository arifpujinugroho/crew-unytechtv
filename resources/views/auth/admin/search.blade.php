@extends('layout.master')

@section('title')
Search
@endsection

@section('header')
    <!-- Notification.css -->
    <link rel="stylesheet" type="text/css" href="assets/pages/notification/notification.css">
@endsection

@section('content')
<!-- Page-header start -->
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="icofont  icofont-stock-search bg-c-yellow"></i>
                <div class="d-inline">
                    <h4>Temukan Crews UNYtechTV</h4>
                    <span>Pencarian Data Crew UKMF UNYtechTV</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ url('/') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Search Crew</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Page-header end -->
<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <!-- Search result card start -->
            <div class="card">
                <div class="card-block">
                    <div class="row">
                        <div class="col-lg-6 offset-lg-3">
                            <p class="txt-highlight text-center m-t-20">Data crew yang dapat ditemukan adalah Mahasiswa yang sudah secara <strong>resmi</strong> menjadi Crew UNYtechTV.
                            </p>
                        </div>
                    </div>
                    <div class="row seacrh-header">
                        <div class="col-lg-4 offset-lg-4 offset-sm-3 col-sm-6 offset-sm-1 col-xs-12">
                            <form method="POST" action="{{ url('/admin/search') }}">
                                {{ csrf_field() }}
                                <div class="input-group input-group-button input-group-primary">
                                    <input type="text" name="keyword" class="form-control" placeholder="Search Nama atau NIM" required>
                                    <button type="submit" class="btn btn-primary input-group-addon">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Search result card end -->

        </div>
    </div>
</div>
@endsection

@section('footer')
    <!-- notification js -->
    <script type="text/javascript" src="assets/js/bootstrap-growl.min.js"></script>
@endsection