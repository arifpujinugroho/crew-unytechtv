@extends('layout.master')

@section('title')
Search
@endsection

@section('header')
    <!-- Notification.css -->
    <link rel="stylesheet" type="text/css" href="assets/pages/notification/notification.css">
@endsection

@section('content')
<!-- Page-header start -->
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <a href="{{url('admin/search-crew')}}">
                    <i class="icofont  icofont-stock-search bg-c-yellow"></i>
                </a>
                <div class="d-inline">
                    <h4>Temukan Crews UNYtechTV</h4>
                    <span class="text-danger"><strong>Klik Logo Search untuk kembali ke pencarian</strong></span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="{{ url('/admin') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ url('admin/search-crew') }}">Search Crew</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Value</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Page-header end -->
<div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <!-- Search result card start -->
            <div class="card">
                <div class="card-block">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nim</th>
                                    <th>Nama Lengkap</th>
                                    <th>Status Keanggotaan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php $c=1; ?>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $c }}</td>
                                        <td>{{ $user->nim }}</td>
                                        <td>{{ $user->namalengkap }}</td>
                                        <td>{{ $user->status_crew }}</td>
                                        <td>
                                            <a href="{{ url ('crew/'.$user->nim) }}" data-toggle="tooltip" title="Lihat"><i class="icofont icofont-eye-alt text-c-blue f-16"></i> Lihat</a>
                                        </td>
                                    </tr>
                                    <?php $c++; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $users->links() }}
                </div>
            </div>
            <!-- Search result card end -->

        </div>
    </div>
</div>
@endsection

@section('footer')
    <!-- notification js -->
    <script type="text/javascript" src="assets/js/bootstrap-growl.min.js"></script>
@endsection