@extends('layout.master')

@section('title')
{{Auth::user()->mahasiswa->namapanggilan}} || Aktivitas Kegiatan
@endsection

@section('content')
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-speed-meter bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>Aktivitas Kegiatan Mahasiswa</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{url('/')}}"> <i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Aktivitas</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->

    <!-- Page-body start -->
    <div class="page-body">
        <div class="row">
            <!-- Current Progress Start -->
            <div class="col-sm-12">
                    <!-- Basic Button table start -->
                    <div class="card">
                        <div class="card-header">
                            <h3>Kegiatan</h3>
                            <span>Jumlah kegiatan yang pernah diikuti : {{ $jml}}</span>
                            <hr>
                            <br>
                            <form action="{{ url()->current() }}">
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <input type="text" name="keyword" class="form-control" placeholder="Search..." value="{{ request('keyword') }}">
                                    </div>
                                    <div class="col">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-block">
                            <div class="dt-responsive table-responsive">
                                <table class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <th>Nama Kegiatan</th>
                                        <th>Jenis Kegiatan</th>
                                        <th>Tanggal Kegiatan</th>
                                    </thead>
                                    <tbody>
                                        @foreach ($tasks as $t)
                                            <tr>
                                                <td class="text-capitalize">{{ $t->nama_kegiatan }}</td>
                                                <td class="text-capitalize">{{ $t->jenis_kegiatan }}</td>
                                                <td class="text-capitalize">{{ $t->tanggal_kegiatan }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $tasks->links() }}
                            </div>
                        </div>
                    </div>
                    <!-- Basic Button table end -->
                </div>
        </div>
        <!-- =========== widget 2  are work end =================   -->
    </div>
    <!-- Page-body end -->
@endsection