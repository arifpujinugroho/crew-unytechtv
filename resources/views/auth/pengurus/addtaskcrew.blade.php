@extends('layout.master')

@section('title')
{{$data->nama_kegiatan}}
@endsection

@section('content')
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-speed-meter bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>{{$data->nama_kegiatan}}</h4>
                        <span><b class="text-danger"> Tanggal Kegiatan :</b> {{$data->tanggal_kegiatan}}</span>
                        <span>&nbsp;&nbsp;<b class="text-danger">Nomer Surat :</b> {{$data->nomer_surat}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{url('/')}}"> <i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{url('admin/kegiatan')}}">Kegiatan</a> </li>
                        <li class="breadcrumb-item"><a href="#!">{{$data->id}}</a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->
@if (session('adminaddtaskcrew'))
<div class="alert alert-danger">
    {{ session('adminaddtaskcrew') }}
</div>
@endif
    <!-- Page-body start -->
    <div class="page-body">
        <div class="row">
            <!-- Current Progress Start -->
            <div class="col-md-12">
                <div class="card card-current-prog">
                    <div class="card-header">
                        <div class="card-header-left">
                            <h5>Crews yang akan bertugas</h5>
                        </div>
                        @if ($jmlcrew > 0)
                            <div class="card-header-right">
                                <a href="{{url('admin/kegiatan/'.$data->id.'/print')}}" target="_blank"><button class="btn-round btn btn-warning btn-sm"><i class="icofont icofont-print"></i> Print</button></a>
                            </div>
                        @else
                            <div class="card-header-right">
                                <button class="btn btn-default btn-sm btn-disabled btn-round disabled">Print</button>
                            </div>
                        @endif
                    </div>
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>NIM</th>
                                    <th>Prodi</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                   <?php $n=1; ?>
                                    @foreach ($crew as $c)
                                        <tr>
                                            <td>{{$n}}</td>
                                            <td>{{$c->namalengkap}}</td>
                                            <td>{{$c->nim}}</td>
                                            <td>{{$c->nama_prodi}}</td>
                                            <td>
                                                <form action="{{ url('admin/delcrew') }}" method="post">
                                                @csrf
                                                <input type="hidden" name="nim" value="{{$c->nim}}">
                                                <input type="hidden" name="idkegiatan" value="{{$data->id}}">
                                                <div class="btn-group">
                                                    <button type="submit" class="btn btn-danger btn-mini"><i class="icofont icofont-ui-delete"></i> Hapus</button>
                                                </div>
                                            </form>
                                            </td>
                                        </tr>
                                    <?php $n++; ?>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-header-left">
                            <h5>Daftar Mahasiswa</h5><br>
                        </div>
                        <div class="card-header-right">
                            <div class="btn-group">
                                    <button class="btn btn-mini btn-primary f-right tambah-ft" data-toggle="tooltip" title="Tambah Mahasiswa FT">Add FT</button>
                                    <button class="btn btn-mini btn-danger f-right disabled"  data-toggle="tooltip" title="Tambah Mahasiswa Non FT" disabled>Add NON FT</button>
                            </div>
                        </div>
                        <form action="{{ url()->current() }}">
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <input type="text" name="keyword" class="form-control form-control-sm" placeholder="Search..." value="{{ request('keyword') }}">
                                    </div>
                                    <div class="col">
                                        <button type="submit" class="btn btn-primary btn-mini">Search</button>
                                    </div>
                                </div>
                            </form>
                    </div>
                    <div class="card-block widget-last-task">
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <th>Nama Panjang</th>
                                        <th>Nama Panggilan</th>
                                        <th>NIM</th>
                                        <th>Prodi</th>
                                        @if($jmlcrew < 7)
                                        <th>Action</th>
                                        @endif
                                    </thead>
                                    <tbody>
                                        @foreach ($mhs as $m)
                                            <tr>
                                                <td><small>{{$m->namalengkap}}</small></td>
                                                <td><small>{{$m->namapanggilan}}</small></td>
                                                <td><small>{{$m->nim}}</small></td>
                                                <td><small>{{$m->nama_prodi}}</small></td>
                                                @if($jmlcrew < 7)
                                                <td>
                                                    <form action="{{ url('admin/addcrew') }}" method="post">
                                                        @csrf
                                                            <input type="hidden" name="idkegiatan" value="{{$data->id}}"> 
                                                            <input type="hidden" name="nim" value="{{$m->nim}}">
                                                    <button type="submit" class="btn btn-mini btn-success" data-toggle="tooltip" title="tambah {{$m->namapanggilan}}"> + </button>
                                                    </form>
                                                </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $mhs->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Task card end -->
        </div>
        <!-- =========== widget 2  are work end =================   -->
    </div>
    <!-- Page-body end -->
@endsection

@section('end')
<div class="modal fade" id="tambah-Modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Mahasiswa FT</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form action="{{url('admin/addcrewft')}}" method="post">
                @csrf
                <input type="hidden" name="idkegiatan" value="{{$data->id}}">                   
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label">Nama Lengkap <strong><span class="text-danger">*</span></strong></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                            <input type="text" name="nama_lengkap" class="form-control form-control-capitalize" placeholder="Nama Lengkap" autofocus="autofocus" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label">NIM <strong><span class="text-danger">*</span></strong></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                            <input type="text" name="nim" class="form-control" minlength="11" maxlength="11" placeholder="NIM" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label">Jenis Kelamin <strong><span class="text-danger">*</span></strong></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                            <select name="jns_kel" class="form-control" required>
                                <option value="">-- Jenis Kelamin --</option>
                                <option value="L">Laki-Laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label">Program Studi <strong><span class="text-danger">*</span></strong></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                            <select name="prodi" class="form-control" required>
                                <option value="">-- Prodi --</option>
                                @foreach($prodi as $p)
					                <option value="{{ $p->id }}">{{ $p->nama_prodi }}</option>
					            @endforeach
                            </select>
                        </div>
                    </div>
                </div>            
            </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect waves-light btn-disabled disabled">Save Task</button>
                    <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="tambah-NonFT" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Mahasiswa NON FT</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form action="{{url('admin/addcrewtask')}}" method="post">
                @csrf
                <input type="hidden" name="idkegiatan" value="{{$data->id}}">                   
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label">Nama Lengkap <strong><span class="text-danger">*</span></strong></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                            <input type="text" name="nama_lengkap" class="form-control form-control-capitalize" placeholder="Nama Lengkap" autofocus="autofocus" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label">NIM <strong><span class="text-danger">*</span></strong></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                            <input type="text" name="nim" class="form-control" minlength="11" maxlength="11" placeholder="NIM" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label">Jenis Kelamin <strong><span class="text-danger">*</span></strong></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                            <select name="jns_kel" class="form-control" required>
                                <option value="">-- Jenis Kelamin --</option>
                                <option value="L">Laki-Laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label">Fakultas <strong><span class="text-danger">*</span></strong></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                            <select name="jns_kel" class="form-control" required>
                                <option value="">-- Fakultas --</option>
                                <option value="FMIPA">FMIPA</option>
                                <option value="FIP">FIP</option>
                                <option value="FE">FE</option>
                                <option value="FIS">FIS</option>
                                <option value="FIK">FIK</option>
                                <option value="FBS">FBS</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-4 col-lg-2 col-form-label">Program Studi <strong><span class="text-danger">*</span></strong></label>
                    <div class="col-sm-8 col-lg-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                            <input type="text" class="form-control" name="prodi" placeholder="Nama Prodi Lengkap" required>
                            </select>
                        </div>
                    </div>
                </div>            
            </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect waves-light btn-disabled disabled">Save Task</button>
                    <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('footer')
<Script type="text/javascript">
$('.tambah-ft').click(function() {
    $('#tambah-Modal').modal('show');
});
$('.tambah-nonft').click(function() {
    $('#tambah-NonFT').modal('show');
});
</Script>

@endsection