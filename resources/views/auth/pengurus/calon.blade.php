@extends('layout.master')

@section('title') 
 Calon Crews {{$thnorgn}}
@endsection

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="card-header-right">
                Mode PCB {{$thnorgn}} : &nbsp;
                @if($modepcb == "false")
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger btn-mini mode-crew">OFF</button>
                        <button type="button" class="btn btn-danger btn-mini disabled">&nbsp;&nbsp;&nbsp;</button>
                    </div>
                @else
                    <div class="btn-group">
                        <button type="button" class="btn btn-success btn-mini disabled">&nbsp;&nbsp;&nbsp;</button>
                        <button type="button" class="btn btn-success btn-mini mode-crew">ON</button>
                    </div>
                @endif
            </div>
            <div class="card-header-left">
                <br>
                <h5>Jumlah Calon Crew {{$jml}}</h5><br>
            </div>
            <form action="{{ url()->current() }}">
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <input type="text" name="keyword" class="form-control form-control-sm" placeholder="Search..." value="{{ request('keyword') }}">
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-primary btn-mini">Search</button>
                        </div>
                    </div>
                </form>
        </div>
        <div class="card-block widget-last-task">
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>Nama Panjang</th>
                            <th>Nama Panggilan</th>
                            <th>NIM</th>
                            <th>Prodi</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach ($mhs as $m)
                                <tr>
                                    <td><small>{{$m->namalengkap}}</small></td>
                                    <td><small>{{$m->namapanggilan}}</small></td>
                                    <td><small>{{$m->nim}}</small></td>
                                    <td><small>{{$m->nama_prodi}}</small></td>
                                    <td>
                                        <button class="btn btn-mini btn-success tambah-pengurus" data-namalengkap="{{$m->namalengkap}}" data-nimmhs="{{$m->nim}}" data-prodimhs="{{$m->nama_prodi}}" data-toggle="tooltip" title="tambah {{$m->namapanggilan}}"> + </button>
                                        
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $mhs->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script type="text/javascript">
/*$('.mode-crew').click(function(){
    console.log("ON status");
});
*/
    $(document).on('click', '.mode-crew', function () {
    var toggle_btn = $(this);
    var state = $(this).html();
        if (state == 'ON') {
            $.get( "{{ url('/pengurus/pcb-down') }}", function() {
                toggle_btn.parent().html('<button type="button" class="btn btn-danger btn-mini mode-crew">OFF</button><button type="button" class="btn btn-danger btn-mini disabled" disabled>&nbsp;&nbsp;&nbsp;</button>');
                console.log("Berhasil di matikan");
            });
        } else {
            $.get("{{ URL('/pengurus/pcb-up') }}")
            .done(function() {
                toggle_btn.parent().html('<button type="button" class="btn btn-success btn-mini disabled" disabled>&nbsp;&nbsp;&nbsp;</button><button type="button" class="btn btn-success btn-mini mode-crew">ON</button>');
                console.log("Berhasil di nyalakan");
            });
        };
    });

</script>
@endsection