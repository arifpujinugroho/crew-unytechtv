@extends('layout.master')
@section('title') 
 Pengelola Website
@endsection
 
@section('content')
<div class="page-body">
    <div class="row">            
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-header-left">
                        <h5>Daftar Mahasiswa</h5><br>
                    </div>
                    <form action="{{ url()->current() }}">
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <input type="text" name="keyword" class="form-control form-control-sm" placeholder="Search..." value="{{ request('keyword') }}">
                                </div>
                                <div class="col">
                                    <button type="submit" class="btn btn-primary btn-mini">Search</button>
                                </div>
                            </div>
                        </form>
                </div>
                <div class="card-block widget-last-task">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <th>Nama Panjang</th>
                                    <th>Nama Panggilan</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                    @foreach ($mhs as $m)
                                        <tr>
                                            <td>{{$m->namalengkap}}</td>
                                            <td>{{$m->namapanggilan}}</td>
                                            <td>
                                                @if ($jmlkelola <= 6 )
                                                <form action="{{ url('pengurus/addpengelola') }}" method="post">
                                                    @csrf
                                                        <input type="hidden" name="nim" value="{{$m->nim}}">
                                                <button type="submit" class="btn btn-mini btn-primary" data-toggle="tooltip" title="tambah pengelola"><i class="icofont icofont-worker "></i> Pengelola</button>
                                                </form>
                                                @endif
                                                <br>
                                                @if ($jmlpreview <= 24 )
                                                <form action="{{ url('pengurus/addpreview') }}" method="post">
                                                    @csrf
                                                        <input type="hidden" name="nim" value="{{$m->nim}}">
                                                <button type="submit" class="btn btn-mini btn-success" data-toggle="tooltip" title="tambah preview"><i class="icofont icofont-read-book-alt"></i>Preview</button>
                                                </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $mhs->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Data widget start -->
        <div class="col-md-12 col-xl-6">
            <div class="card project-task">
                <div class="card-header">
                    <div class="card-header-left ">
                        <h5>Pengelola Web Utama</h5>
                    </div>
                    <div class="card-header-right ">
                       <small class="text-muted">Total : {{$jmlkelola}}/7</small>
                    </div>
                </div>
                <div class="card-block p-b-10">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama </th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="task-contain">
                                            <p>1</p>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="task-contain">
                                            <p><b>{{Auth::user()->mahasiswa->namalengkap }}</b></p>
                                        </div>
                                    </td>
                                    <td>
                                        <p class="d-inline-block m-r-20"><small>(saya)</small></p>
                                    </td>
                                </tr>
                                
                                <?php $n=2; ?>
                                @foreach ($kelola as $k)
                                    <tr>
                                        <td>
                                            <div class="task-contain">
                                                <p>{{ $n }}</p>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="task-contain">
                                                <p><b>{{$k->namalengkap}}</b></p>
                                            </div>
                                        </td>
                                        <td>
                                            <form action="{{url('pengurus/delpengelola')}}" method="post">
                                                @csrf
                                                <input type="hidden" name="nim" value="{{$k->username}}">
                                                <button class="btn btn-mini btn-danger" data-toggle="tooltip" title="Hapus" type="submit">x</button>
                                            </form>
                                        </td>
                                    </tr>
                                <?php $n++; ?>    
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-xl-6">
            <div class="card project-task">
                <div class="card-header">
                    <div class="card-header-left ">
                        <h5>Preview Crews UNYtechTV</h5>
                    </div>
                    <div class="card-header-right ">
                       <small class="text-muted">Total : {{$jmlpreview}}/25</small>
                    </div>
                </div>
                <div class="card-block p-b-10">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama </th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $p=1; ?>
                                @foreach ($preview as $pr)
                                <tr>
                                    <td>
                                        <div class="task-contain">
                                            <p>{{ $p }}</p>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="task-contain">
                                            <p><b>{{ $pr->namalengkap }}</b></p>
                                        </div>
                                    </td>
                                    <td>
                                        <form action="{{url('pengurus/delpengelola')}}" method="post">
                                            @csrf
                                            <input type="hidden" name="nim" value="{{$pr->username}}">
                                            <button class="btn btn-mini btn-danger" data-toggle="tooltip" title="Hapus" type="submit">x</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Data widget End -->
    </div>
</div>
@endsection