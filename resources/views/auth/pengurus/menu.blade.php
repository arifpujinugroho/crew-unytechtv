@extends('layout.master')

@section('title')
    Section Crew
@endsection

@section('content')
        <!-- Page-body start -->
        <div class="page-body">
            <div class="row">
                <!-- Weather Start -->
                <div class="col-md-12 col-xl-4" id="tambahcrewblock">
                    <div class="card ">
                        <div class="card-header ">
                            <div class="card-header-left ">
                                <h5>Tambah Crew Kegiatan</h5>
                            </div>
                        </div>
                        <div class="card-block ">
                            @if($jmlcrew < 7)
                                <div class="form-group">
                                    <form action="{{ url('admin/addcrew') }}" method="post">
                                        @csrf
                                        <div class="input-group">
                                            <input type="hidden" name="idkegiatan" value="{{$data->id}}"> 
                                            <input type="text" name="nim" id="nim" class="form-control" placeholder="NIM Crew">
                                        </div>
                                        <button type="submit" class="btn btn-sm btn-success f-right">Tambah Crews</button>
                                    </form>
                                </div>
                                <br>
                                <br>
                                <hr>
                            @endif
                            <div class="form-group">
                                <label for="tambah mahasiswa">Jika mahasiswa belum terdaftar</label>
                                <button class="btn btn-warning" data-toggle="modal" data-target="#tambah-Modal">Tambah Mahasiswa</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Weather End -->

                
                <!-- Weather Start -->
                <div class="col-md-12 col-xl-4" id="tambahcrewblock">
                    <div class="card ">
                        <div class="card-header ">
                            <div class="card-header-left ">
                                <h5>Tambah Crew Kegiatan</h5>
                            </div>
                        </div>
                        <div class="card-block ">
                            @if($jmlcrew < 7)
                                <div class="form-group">
                                    <form action="{{ url('admin/addcrew') }}" method="post">
                                        @csrf
                                        <div class="input-group">
                                            <input type="hidden" name="idkegiatan" value="{{$data->id}}"> 
                                            <input type="text" name="nim" id="nim" class="form-control" placeholder="NIM Crew">
                                        </div>
                                        <button type="submit" class="btn btn-sm btn-success f-right">Tambah Crews</button>
                                    </form>
                                </div>
                                <br>
                                <br>
                                <hr>
                            @endif
                            <div class="form-group">
                                <label for="tambah mahasiswa">Jika mahasiswa belum terdaftar</label>
                                <button class="btn btn-warning" data-toggle="modal" data-target="#tambah-Modal">Tambah Mahasiswa</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Weather End -->
            </div>
            <!-- =========== widget 2  are work end =================   -->
        </div>
        <!-- Page-body end -->
@endsection