@extends('layout.master')

@section('title')
Pengurus Organisasi
@endsection

@section('header')

@endsection

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="card-header-left">
                <h5>Daftar Mahasiswa</h5><br>
            </div>
            <div class="card-header-right">
                <div class="btn-group">
                </div>
            </div>
            <form action="{{ url()->current() }}">
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <input type="text" name="keyword" class="form-control form-control-sm" placeholder="Search..." value="{{ request('keyword') }}">
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-primary btn-mini">Search</button>
                        </div>
                    </div>
                </form>
        </div>
        <div class="card-block widget-last-task">
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>Nama Panjang</th>
                            <th>Nama Panggilan</th>
                            <th>NIM</th>
                            <th>Prodi</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach ($mhs as $m)
                                <tr>
                                    <td><small>{{$m->namalengkap}}</small></td>
                                    <td><small>{{$m->namapanggilan}}</small></td>
                                    <td><small>{{$m->nim}}</small></td>
                                    <td><small>{{$m->nama_prodi}}</small></td>
                                    <td>
                                        <button class="btn btn-mini btn-success tambah-pengurus" data-namalengkap="{{$m->namalengkap}}" data-nimmhs="{{$m->nim}}" data-prodimhs="{{$m->nama_prodi}}" data-toggle="tooltip" title="tambah {{$m->namapanggilan}}"> + </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $mhs->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-xl-12">
    <div class="card recent-candidate-card">
        <div class="card-header p-b-10">
            <div class="card-header-left">
            <h5>Pengurus UKMF UNYtechTV {{$thnorgn}}</h5>
            <span>Jumlah pengurus : {{$jml}}</span>
            </div>
        </div>
        <div class="card-block p-t-0 p-b-0">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nama Pengurus</th>
                            <th>Jabatan</th>
                            <th>Prodi</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    @if($jml == 0 )
                        <tr>
                            <td>
                                <a href="#!">
                                    <img class="img-rounded" src="{{url('assets/images/avatar-blank.jpg')}}" alt="">
                                </a>
                                <div class="recent-contain">
                                    <h6><b>Nama Lengkap</b></h6>
                                    <p class="text-muted">NIM</p>
                                </div>
                            </td>
                            <td>
                                <p class="m-l-10 d-inline-block f-w-600"><i><b>Posisi Jabatan Pengurus</b></i></p>
                            </td>
                            <td>
                                <p class="m-l-10 d-inline-block f-w-600">Program Studi</p>
                            </td>
                            <td>
                            <a href="#!" data-toggle="tooltip" title="Lihat" style="cursor: not-allowed;" class="text-muted"><i class="icofont icofont-eye-alt text-muted f-16"></i> Lihat</a>
                            </td>
                        </tr> 
                    @else
                        @foreach ($data as $d)
                            <tr>
                                <td>
                                    <a data-toggle="tooltip" title="Ganti Foto Resmi {{ $d->namapanggilan }}" class="foto-resmi" data-nim="{{$d->nim}}" data-namalengkap="{{$d->namalengkap}}">
                                        @if($d->fotoresmi == "NULL" || $d->fotoresmi == "")
                                            <img class="img-50 img-rounded" src="{{url('storage/foto/'.$d->tahunpcb.'/client/'.$d->fotoawal)}}" alt="Foto {{$d->namapanggilan}}">
                                        @else
                                            <img class="img-50 img-rounded" src="{{url('storage/foto/'.$d->tahunpcb.'/resmi/'.$d->fotoresmi)}}" alt="Foto {{$d->namapanggilan}}">
                                        @endif
                                    </a>
                                    <div class="recent-contain">
                                        <h6><b>{{ $d->namalengkap }}</b></h6>
                                        <p class="text-muted">{{ $d->nim }}</p>
                                    </div>
                                </td>
                                <td>
                                    <p class="m-l-10 d-inline-block f-w-600"><i><b>{{ $d->posisi_lengkap }}</b></i></p>
                                </td>
                                <td>
                                    <p class="m-l-10 d-inline-block f-w-600">{{ $d->nama_prodi }}</p>
                                </td>
                                <td style="cursor: pointer;">
                                <form action="{{url('pengurus/hapuspengurus')}}" method="post">
                                @csrf
                                    <input type="hidden" name="nim" value="{{$d->nim}}">
                                    <button type="submit" class="btn btn-mini btn-danger" data-toggle="tooltip" title="Hapus Pengurus">X</button>    
                                </form>
                                </td>
                            </tr>
                        @endforeach
                    @endif                                            
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('end')
<div class="modal fade" id="tambah-Pengurus" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Pengurus {{$thnorgn}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form action="{{url('pengurus/tambahpengurus')}}" method="post">
                @csrf                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <input type="text" name="namalengkap" id="nama-lengkap" class="form-control" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="input-group">
                                <input type="text" name="nim" id="nim-mhs" class="form-control" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="input-group">
                                <input type="text" id="prodi-mhs" class="form-control" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="input-group">
                                <select name="posisi" class="form-control" required>
                                    <option value="">-- Posisi Pengurus --</option>
                                    @foreach($struktur as $s)
                                        <option value="{{ $s->id }}">{{ $s->nama_pendek }}</option>
                                    @endforeach
                                </select>
                        </div>
                    </div>
                </div>            
            </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect waves-light">Save</button>
                    <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="foto-Resmi" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Foto Resmi</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form action="{{url('pengurus/fotoresmi')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="nim" id="nim-mhsiswa">                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="input-group">
                            <input type="text" name="namalengkap" id="detail-namalengkap" class="form-control" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="input-group">
                                <input type="file" id="file" name="foto" class="form-control" required>
                        </div>
                    </div>
                </div>                    
            </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect waves-light">Save</button>
                    <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>    
@endsection

@section('footer')
<script>
        (function($) {
            $.fn.checkFileType = function(options) {
                var defaults = {
                    allowedExtensions: [],
                    success: function() {},
                    error: function() {}
                };
                options = $.extend(defaults, options);
        
                return this.each(function() {
        
                    $(this).on('change', function() {
                        var value = $(this).val(),
                            file = value.toLowerCase(),
                            extension = file.substring(file.lastIndexOf('.') + 1);
        
                        if ($.inArray(extension, options.allowedExtensions) == -1) {
                            options.error();
                            $(this).focus();
                        } else {
                            options.success();
        
                        }
        
                    });
        
                });
            };
        
        })(jQuery);
        
        var uploadField = document.getElementById("file");
        uploadField.onchange = function() {
            if(this.files[0].size > 5055650){
                new PNotify({
                        title: 'File Oversize',
                        text: 'Maaf, File Max 5MB ',
                        type: 'error'
                });
                console.log("file size = " + this.files[0].size + "/5055650")
                this.value = "";
            };
        };
        
        $(function() {
            $('#file').checkFileType({
                allowedExtensions: ['jpg', 'jpeg','png'],
                error: function() {
                    new PNotify({
                        title: 'File not Image',
                        text: 'Maaf, hanya type image yang diupload ',
                        type: 'error'
                    });
                    document.getElementById("file").value = "";
                }
            });
        });
</script>

    <script>
        $('.tambah-pengurus').click(function() {
            $('#nama-lengkap').val($(this).data('namalengkap'));
            $('#nim-mhs').val($(this).data('nimmhs'));
            $('#prodi-mhs').val($(this).data('prodimhs'));
            $('#tambah-Pengurus').modal('show');
        });

        $('.foto-resmi').click(function() {
            $('#detail-namalengkap').val($(this).data('namalengkap'));
            $('#nim-mhsiswa').val($(this).data('nim'));
            $('#foto-Resmi').modal('show');
        });
    </script>
@endsection