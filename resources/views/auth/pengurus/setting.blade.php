@extends('layout.master')
@section('title') 
 Setting
@endsection

@section('content')
<div class="page-body">
<div class="row">
    <div class="col-sm-12">
        <!-- Panel card start -->
        <div class="card">
            <div class="card-header">
                <h5>Management Anggota</h5>
            </div>
            <div class="card-block panels-wells">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="panel panel-success">
                            <div class="panel-heading bg-success">
                                    Mode Penerimaan Crew Baru
                            </div>
                            <div class="panel-body">
                            </div>
                            <div class="panel-footer text-success">
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel panel-success">
                            <div class="panel-heading bg-success">
                                    &nbsp;
                            </div>
                            <div class="panel-body">
                                
                            </div>
                            <div class="panel-footer text-success">
                                
                            </div>
                        </div>
                    </div>
                    <!-- end of row -->
                </div>
            </div>
        </div>
        <!-- Panel card end -->
    </div>
</div>
<hr>
<hr>
<div class="card-block panels-wells">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-danger">
                <div class="panel-heading bg-danger">
                    Atur tahun kepengurusan yang aktif
                </div>
                <div class="panel-body text-danger">
                    <strong>Tambah tahun kepengurusan</strong>
                    <form action="#" method="post">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" name="tahun_anggaran" class="form-control" placeholder="Tahun Kepengurusan">
                            </div>
                            <p class="help-block"><strong>Format: YYYY (Contoh: 2014).</strong></p>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <textarea class="form-control" rows="3" name="keterangan" placeholder="Keterangan" required></textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-danger pull-right next-step">Simpan <i class="fa fa-save"></i></button>
                    </form>
                    <br>
                    <strong class="text-warning">List Tahun Kepengurusan (Jingga = Aktif)</strong>
                    <div class="table-responsive text-dark">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td>Status</td>
                                    <td>Tahun</td>
                                    <td>Nama Kepengurusan</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tahun_kepengurusan as $tahun)
                                <tr class="{{ ($tahun->aktif == 1) ? 'warning' : '' }}">
                                    <td>
                                        <form action="#" method="post">
                                        <input type="hidden" value="{{ $tahun->id }}" name="id">
                                        <button type="submit" class="btn btn-{{ ($tahun->aktif != 1) ? 'default' : 'warning' }} btn-mini">&nbsp;<i class="icofont icofont-star"></i></button>
                                        </form>
                                    </td>
                                    <td>{{ $tahun->tahun }}</td>
                                    <td>{{ $tahun->nama }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="panel-footer text-danger">
                   <strong>Hati-hati dalam membuat dan memilih tahun kepengurusan</strong>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection