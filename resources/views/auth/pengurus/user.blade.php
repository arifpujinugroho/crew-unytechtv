@extends('layout.master')

@section('title') 
 User Keseluruhan UNYtechTV
@endsection

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <div class="card-header-right">
            </div>
            <div class="card-header-left">
                <br>
                <h5>Jumlah Keseluruhan User = {{$jml}}</h5><br>
            </div>
            <form action="{{ url()->current() }}">
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <input type="text" name="keyword" class="form-control form-control-sm" placeholder="Search..." value="{{ request('keyword') }}">
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-primary btn-mini">Search</button>
                        </div>
                    </div>
                </form>
        </div>
        <div class="card-block widget-last-task">
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>Nama Panjang</th>
                            <th>Nama Panggilan</th>
                            <th>NIM</th>
                            <th>Prodi</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach ($mhs as $m)
                                <tr>
                                    <td><small>{{$m->namalengkap}}</small></td>
                                    <td><small>{{$m->namapanggilan}}</small></td>
                                    <td><small>{{$m->nim}}</small></td>
                                    <td><small>{{$m->nama_prodi}}</small></td>
                                    <td>
                                        <div class="btn-group">
                                            <form action="reset-pass" method="post">
                                                @csrf
                                                <input type="hidden" name="nim" value="{{$m->nim}}">
                                                <button type="submit" class="btn btn-mini btn-success" data-toggle="tooltip" title="Reset pass {{$m->namapanggilan}}"> + </button>
                                            </form>
                                            <form action="hapus-akun" method="post">
                                                @csrf
                                                <input type="hidden" name="nim" value="{{$m->nim}}">
                                                <button type="submit" class="btn btn-mini btn-danger" data-toggle="tooltip" title="Delete akun {{$m->namapanggilan}}"> X </button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $mhs->links() }}
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('footer')
<script type="text/javascript">
</script>
@endsection