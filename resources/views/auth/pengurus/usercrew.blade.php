@extends('layout.master')

@section('title')
    Management Crew
@endsection

@section('content')
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-layout bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>Management Crew</h4>
                         <span>Daftar Calon, Crew dan Alumni UKMF UNYtechTV</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                <li class="breadcrumb-item">
                    <a href="{{url('/')}}">
                        <i class="icofont icofont-home"></i>
                    </a>
                </li>
                <li class="breadcrumb-item"><a href="#!">Crew</a>
                </li>
            </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->

    <!-- Page body start -->
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- Panel card start -->
                <div class="card">
                    <div class="card-header">
                        <h5>Management Anggota</h5>
                    </div>
                    <div class="card-block panels-wells">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="panel panel-success">
                                    <div class="panel-heading bg-success">
                                            Custom Daftar Crew UNYtechTV
                                    </div>
                                    <div class="panel-body">
                                        <p>Cari anggota berdasarkan tahun pendaftaran</p>
                                        <form action="" method="post">
                                            <select name="" id="" class="form-control">
                                                @foreach ($data as $d)
                                                    <option value="{{ $d->tahun }}">{{ $d->tahun }}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                    <div class="panel-footer text-success">
                                        <button type="submit" class="btn btn-sm btn-success">Cari</button>
                                        
                                    </div>
                                </form>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="panel panel-success">
                                    <div class="panel-heading bg-success">
                                        Custom Daftar Alumni UNYtechTV
                                    </div>
                                    <div class="panel-body">
                                        <p>Cari anggota berdasarkan tahun pendaftaran</p>
                                        <form action="" method="post">
                                            <select name="" id="" class="form-control">
                                                <option value=""></option>
                                            </select>
                                        
                                    </div>
                                    <div class="panel-footer text-success">
                                        <button type="submit" class="btn btn-sm btn-success">Cari</button>
                                        
                                    </div>
                                </form>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="panel panel-info">
                                    <div class="panel-heading bg-info">
                                        Lanjutan Daftar Crew UNYtechTV
                                    </div>

                                    <div class="panel-body">
                                        Lihat Daftar Crew UNYtechTV :&#9;
                                        <a href="{{url('pengurus/caloncrew')}}">
                                            <button class="btn btn-success btn-mini">Lihat Calon</button>
                                        </a>
                                        <hr>
                                        Lihat Daftar Seluruh User :&#9;  
                                        <a href="{{url('pengurus/user')}}">
                                            <button class="btn btn-warning btn-mini">Lihat User</button>
                                        </a>
                                        <hr>
                                        Lihat Pengelola Web :&#9;  
                                        <a href="{{url('pengurus/pengelola')}}">
                                            <button class="btn btn-danger btn-mini">Pengelola</button>
                                        </a>
                                    </div>
                                </form>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="panel panel-danger">
                                    <div class="panel-heading bg-danger">
                                        Perhatian
                                    </div>
                                    <div class="panel-body text-danger">
                                        <strong>Perhatikan dalam pemilihan tahun. daftar mahasiswa tercantum disesuaikan dengan tahun saat mereka mendaftar menjadi pengurus</strong>
                                    </div>
                                    <div class="panel-footer text-danger">
                                        &copy;&nbsp;Dev UNYtechTV 2019<
                                    </div>
                                </div>
                            </div>
                            <!-- end of row -->
                        </div>
                    </div>
                </div>
                <!-- Panel card end -->
            </div>
        </div>
    </div>
    <!-- Page body end -->
@endsection