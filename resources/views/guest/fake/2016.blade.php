@extends('layout.master')

@section('title')
PCB {{ $tahun }}
@endsection

@section('header')
    <!-- sweet alert framework -->
    <link rel="stylesheet" type="text/css" href="assets/bower_components/sweetalert/css/sweetalert.css">
@endsection


@section('content')
    {{-- Page-header start --}}
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-file-code bg-c-blue"></i>
                    <div class="d-inline">
                    <h4>DPO 2019</h4>
                        <span>Form DPO 2019</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="{{ url('/home') }}">
                        <i class="icofont icofont-home"></i>
                    </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Regisrtasi</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    {{-- Page-header end --}}
    

    {{-- Page-body start --}}
    <div class="page-body">
        <div class="card">
            <div class="card-header">
                <h5>MOHON ISI DENGAN CERMAT</h5>
            <span>Masalah segera hubungi : <a href="https://wa.me/{{ $hotline }}" target="_blank" class="text-success"><i class="icofont icofont-brand-whatsapp"></i> +{{ $hotline }}</a></span>
            </div>
            <div class="card-block">
            <form method="POST" action="{{ url('/dpo2019') }}" enctype="multipart/form-data">
                @method('POST')
                    @csrf
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Nama Lengkap <strong><span class="text-danger">*</span></strong></label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                                <input type="text" name="nama_lengkap" class="form-control form-control-capitalize" placeholder="Nama Lengkap" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Nama Panggilan <strong><span class="text-danger">*</span></strong></label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                                <input type="text" name="nama_panggilan" class="form-control form-control-capitalize" placeholder="Nama Panggilan" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">NIM <strong><span class="text-danger">*</span></strong></label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                                <input type="text" name="nim" class="form-control" minlength="11" maxlength="11" placeholder="NIM" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Program Studi <strong><span class="text-danger">*</span></strong></label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                                <select name="prodi" class="form-control" required>
                                    <option value="">-- Prodi --</option>
                                    @foreach($prodi as $p)
						                <option value="{{ $p->id }}">{{ $p->nama_prodi }}</option>
						            @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Tempat Lahir <strong><span class="text-danger">*</span></strong></label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                                <input type="text" name="tmp_lahir" class="form-control" placeholder="Tempat Lahir" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Tanggal Lahir <strong><span class="text-danger">*</span></strong></label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                                <input name="tgl_lahir" class="form-control" type="date" placeholder="Tangggal Lahir" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Jenis Kelamin <strong><span class="text-danger">*</span></strong></label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                                <select name="jns_kel" class="form-control" required>
                                    <option value="">-- Jenis Kelamin --</option>
                                    <option value="L">Laki-Laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Agama <strong><span class="text-danger">*</span></strong></label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                                <select name="agama" class="form-control" required>
                                    <option value="">-- Agama --</option>
                                    <option value="Islam">Islam</option>
                                    <option value="Kristen Katolik">Kristen Katolik</option>
                                    <option value="Kristen Protestan">Kristen Protestan</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Budha">Budha</option>
                                    <option value="Kong Hu Cu">Kong Hu Cu</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Alamat<strong><span class="text-danger">*</span></strong></label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                                <textarea type="text" name="alamat" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Kontak <strong><span class="text-danger">*</span></strong></label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                                <input type="tel" name="telepon" class="form-control" placeholder="Telepon atau Whatsapp" required>
                            </div>
                        </div>
                    </div>                    
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Instagram</label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                                <input type="text" name="instagram" class="form-control" placeholder="Instagram">
                            </div>
                        </div>
                    </div>                    
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Pas Foto <strong class="text-danger">*</strong></label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                                <input type="file" name="foto" class="form-control" required>
                            </div>
                        </div>
                    </div>
                            <span>Perhatikan!! tanda ( <strong class="text-danger">*</strong> )  wajib untuk diisi</span>
                            <button type="submit" class="btn btn-success m-b-0 f-right">Daftar</button>
                </form>
            </div>
        </div>
    </div>
    {{-- Page-body end --}}
@endsection

@section('footer')
    <script type="text/javascript" src="assets/bower_components/sweetalert/js/sweetalert.min.js"></script>
    <script type="text/javascript">
        @if (session('status') == "registered")
            $(document).ready(function () {
                swal("User Registered", "Mahasiswa tersebut telah terdaftar", "error");
            });
        @endif
    </script>
@endsection