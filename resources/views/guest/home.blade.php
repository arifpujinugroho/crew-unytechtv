@extends('layout.master')

@section('title')
Home
@endsection

@section('content')
<!-- Page-header start -->
<div class="page-header card">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <a href="{{url('cache')}}"><i class="icofont icofont-clip-board bg-c-yellow"></i></a>
                <div class="d-inline">
                    <h4>Selamat Datang</h4>
                    <span>Halaman Utama Crews Management System</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
            <li class="breadcrumb-item">
                <a href="{{ url('/') }}">
                    <i class="icofont icofont-home"></i>
                </a>
            </li>
        </ul>
            </div>
        </div>
    </div>
</div>
<!-- Page-header end -->

@if (session('status'))
    <div class="alert alert-danger">
        {{ session('status') }}
    </div>
@endif

<div class="page-body">
    <div class="row">
        <!-- total start -->
        <div class="col-md-6 col-xl-3">
            <div class="card widget-card-1">
                <div class="card-block-small">
                    <i class="icofont icofont-pie-chart bg-c-blue card1-icon"></i>
                    <span class="text-c-blue f-w-600">Total Keseluruhan</span>
                    <h4><small>{{ $tmhs }} Mahasiswa</small></h4>
                    <div>
                        <span class="f-left m-t-10 text-muted">
                            <i class="text-c-blue f-16 icofont icofont-refresh m-r-10"></i>Total Crew & Alumni
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- Total end -->
        <!-- crew start -->
        <div class="col-md-6 col-xl-3">
            <div class="card widget-card-1">
                <div class="card-block-small">
                    <i class="icofont icofont icofont-users-alt-2 bg-c-pink card1-icon"></i>
                    <span class="text-c-pink f-w-600">Crews</span>
                    <h4><small>{{ $tcrew }} Crews</small></h4>
                    <div>
                        <span class="f-left m-t-10 text-muted">
                            <i class="text-c-pink f-16 icofont icofont-users-alt-2 m-r-10"></i>Crews UNYtechTV
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- crew end -->
        <!-- alumni start -->
        <div class="col-md-6 col-xl-3">
            <div class="card widget-card-1">
                <div class="card-block-small">
                    <i class="icofont icofont-graduate-alt bg-c-green card1-icon"></i>
                    <span class="text-c-green f-w-600">Alumni</span>
                    <h4><small>{{ $talumni }} Alumni</small></h4>
                    <div>
                        <span class="f-left m-t-10 text-muted">
                            <i class="text-c-green f-16 icofont icofont-graduate-alt m-r-10"></i>Alumni UNYtechTV
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- alumni end -->
        @if($pcb == 'true')
        <!-- calon start -->
        <div class="col-md-6 col-xl-3">
            <div class="card widget-card-1">
                <div class="card-block-small">
                    <i class="icofont icofont-student-alt bg-c-yellow card1-icon"></i>
                    <span class="text-c-yellow f-w-600">Calon {{ $thnorgn }}</span>
                    <h4><small>{{ $tcalon }} Calon</small></h4>
                    <div>
                        <span class="f-left m-t-10 text-muted">
                            <i class="text-c-yellow f-16 icofont-student-alt m-r-10"></i>Calon Crews Tahun {{ $thnorgn }}
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- calon end -->
        @else
        <!-- calon start -->
        <div class="col-md-6 col-xl-3">
            <div class="card widget-card-1">
                <div class="card-block-small">
                    <i class="icofont icofont-architecture-alt bg-c-yellow card1-icon"></i>
                    <span class="text-c-yellow f-w-600">Pengurus</span>
                    <h4><small>{{ $tpengurus }} Pengurus</small></h4>
                    <div>
                        <span class="f-left m-t-10 text-muted">
                            <i class="text-c-yellow f-16 icofont icofont-architecture-alt m-r-10"></i>Pengurus Tahun {{ $thnorgn }}
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- calon end -->
        @endif
    </div>
</div>

@endsection