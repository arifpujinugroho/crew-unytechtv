@extends('layout.master')

@section('title')
{{$data->namapanggilan}}
@endsection

@section('header')

@endsection

@section('content')
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-listine-dots bg-c-lite-green"></i>
                    <div class="d-inline">
                        <h4>Information Crew</h4>
                        <span>Informasi Crew UKMF UNYtech TV yang telah terdaftar</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                     <ul class="breadcrumb-title">
                <li class="breadcrumb-item">
                    <a href="{{ url('/') }}">
                        <i class="icofont icofont-home"></i>
                    </a>
                </li>
                @guest
                    <li class="breadcrumb-item"><a href="{{ url('/search-crew') }}">Search Crew</a>
                    </li>                    
                @else
                    @if(Auth::user()->level == 'admin')
                    <li class="breadcrumb-item"><a href="{{ url('admin/search-crew') }}">Search Crew</a>
                    </li>
                @elseif(Auth::user()->level == 'pengurus')
                    <li class="breadcrumb-item"><a href="{{ url('pengurus/search-crew') }}">Search Crew</a>
                    </li>
                @elseif(Auth::user()->level == 'preview')
                    <li class="breadcrumb-item"><a href="{{ url('preview/search-crew') }}">Search Crew</a>
                    </li>
                @endif   
                @endguest
                
                <li class="breadcrumb-item"><a href="#!">Crew</a>
                </li>
            </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->

    <!-- Page body start -->
    <div class="page-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- Product detail page start -->
                    <div class="card product-detail-page">
                        <div class="card-block">
                            <div class="row">
                                <div class="col-lg-5 col-xs-12">
                                    <div class="port_details_all_img row">
                                        <div class="col-lg-12 m-b-15">
                                            <div id="big_banner">
                                                <div class="port_big_img">
                                                    @if($data->fotoresmi == "NULL" || $data->fotoresmi == "")
                                                        <img class="img img-fluid" src="{{url('storage/foto/'.$data->tahunpcb.'/client/'.$data->fotoawal)}}" alt="Foto {{$data->namapanggilan}}">
                                                    @else
                                                        <img class="img img-fluid" src="{{url('storage/foto/'.$data->tahunpcb.'/resmi/'.$data->fotoresmi)}}" alt="Foto {{$data->namapanggilan}}">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-xs-12 product-detail">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <span class="txt-muted d-inline-block">Tahun Pendaftaran :  {{$data->tahunpcb}} </span>
                                            <span class="f-right text-danger"><strong>{{$data->status_crew}} </strong></span>
                                        </div>
                                        <div>
                                            <div class="col-lg-12">
                                                <h1 class="pro-desc f-w-300"><strong> {{$data->namalengkap}} </strong> <br><small class="text-muted">&nbsp;&nbsp;&nbsp;{{$data->namapanggilan}}</small></h1>
                                            </div>
                                            <div class="col-lg-12">
                                                <span class=""> {{$data->nim}} </span>
                                            </div>
                                        </div>
                                            <div class="col-lg-12">
                                                @if ($status > 0)
                                                    <span class="text-primary product-price">{{$jabatan->posisi_lengkap}}</span> 
                                                    <span class="text-primary">Tahun : {{$jabatan->tahun}}</span>
                                                @endif
                                                <br>
                                                <hr>
                                                <table>
                                                    <tr>
                                                        <th>
                                                            Program Studi :
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            {{$data->nama_prodi}}
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br>
                                                <table>
                                                    <tr>
                                                        <th>
                                                            Jenis Kelamin :
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            @if($data->jenis_kel == "P")
                                                                Perempuan
                                                            @else
                                                                Laki-Laki
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br>
                                                <table>
                                                    <tr>
                                                        <th>
                                                            Agama :
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            {{$data->agama}}
                                                        </td>
                                                    </tr>
                                                </table>
                                                <hr>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Product detail page end -->
                </div>
            </div>
        </div>
        <!-- Page body end -->
@endsection