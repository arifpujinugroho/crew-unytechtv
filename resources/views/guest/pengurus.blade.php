@extends('layout.master')

@section('title')
Pengurus Organisasi
@endsection

@section('header')

@endsection

@section('content')
<div class="col-xl-12">
    <div class="card recent-candidate-card">
        <div class="card-header p-b-10">
            <div class="card-header-left">
            <h5>Pengurus UKMF UNYtechTV {{$thnorgn}}</h5>
            <span>Jumlah pengurus : {{$jml}}</span>
            </div>
            <div class="card-header-right">
                <ul class="list-unstyled card-option">
                    <li><i class="icofont icofont-simple-left "></i></li>
                    <li><i class="icofont icofont-maximize full-card"></i></li>
                    <li><i class="icofont icofont-minus minimize-card"></i></li>
                    <li><i class="icofont icofont-refresh reload-card"></i></li>
                </ul>
            </div>
        </div>
        <div class="card-block p-t-0 p-b-0">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nama Pengurus</th>
                            <th>Jabatan</th>
                            <th>Prodi</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    @if($jml == 0 )
                        <tr>
                            <td>
                                <a href="#!">
                                    <img class="img-rounded" src="assets/images/avatar-blank.jpg" alt="">
                                </a>
                                <div class="recent-contain">
                                    <h6><b>Nama Lengkap</b></h6>
                                    <p class="text-muted">NIM</p>
                                </div>
                            </td>
                            <td>
                                <p class="m-l-10 d-inline-block f-w-600"><i><b>Direktur Utama</b></i></p>
                            </td>
                            <td>
                                <p class="m-l-10 d-inline-block f-w-600">Program Studi</p>
                            </td>
                            <td>
                                <a href="#!" data-toggle="tooltip" title="Lihat" style="cursor: not-allowed;" class="text-muted"><i class="icofont icofont-eye-alt text-muted f-16"></i> Lihat</a>
                            </td>
                        </tr> 
                    @else
                        @foreach ($data as $d)
                            <tr>
                                <td>
                                    <a href="{{ url('/crew') }}/{{$d->nim}}">
                                        @if($d->fotoresmi == "NULL" || $d->fotoresmi == "")
                                            <img class=" img-50 img-rounded" src="{{url('storage/foto/'.$d->tahunpcb.'/client/'.$d->fotoawal)}}" alt="Foto {{$d->namapanggilan}}">
                                        @else
                                            <img class="img-50 img-rounded" src="{{url('storage/foto/'.$d->tahunpcb.'/resmi/'.$d->fotoresmi)}}" alt="Foto {{$d->namapanggilan}}">
                                        @endif
                                    </a>
                                    <div class="recent-contain">
                                        <h6><b>{{ $d->namalengkap }}</b></h6>
                                        <p class="text-muted">{{ $d->nim }}</p>
                                    </div>
                                </td>
                                <td>
                                    <p class="m-l-10 d-inline-block f-w-600"><i><b>{{ $d->posisi_lengkap }}</b></i></p>
                                </td>
                                <td>
                                    <p class="m-l-10 d-inline-block f-w-600">{{ $d->nama_prodi }}</p>
                                </td>
                                <td>
                                <a href="{{ url('/crew') }}/{{$d->nim}}" data-toggle="tooltip" title="Lihat"><i class="icofont icofont-eye-alt text-c-blue f-16"></i> Lihat</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif                                            
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection