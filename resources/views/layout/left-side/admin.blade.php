<li class="{{ (Request::path() == 'admin/home') ? 'active' : '' }}">
    <a href="{{ url ('admin')}}">
    <span class="pcoded-micon"><i class="ti-home"></i><b>H</b></span>
    <span class="pcoded-mtext" data-i18n="nav.dash.main">Home</span>
    <span class="pcoded-mcaret"></span>
    </a>
</li>
<li class="{{ (Request::path() == 'admin/search-crew') ? 'active' : '' }}">
    <a href="{{ url ('admin/search-crew')}}">
        <span class="pcoded-micon"><i class="ti-search"></i><b>S</b></span>
        <span class="pcoded-mtext" data-i18n="nav.search.main">Temukan Crew</span>
        <span class="pcoded-mcaret"></span>
    </a>
</li>
<li class="{{ (Request::path() == 'admin/kegiatan') ? 'active' : '' }}">
    <a href="{{ url ('admin/kegiatan')}}">
        <span class="pcoded-micon"><i class="ti-layout-media-right"></i><b>T</b></span>
        <span class="pcoded-mtext" data-i18n="nav.invoice.main">Kegiatan</span>
        <span class="pcoded-mcaret"></span>
    </a>
</li>
<li class="{{ (Request::path() == 'admin/pengelola') ? 'active' : '' }}">
    <a href="{{ url ('admin/pengelola')}}">
        <span class="pcoded-micon"><i class="ti-user"></i><b>P</b></span>
        <span class="pcoded-mtext" data-i18n="nav.user-profile.main">Pengelola</span>
        <span class="pcoded-mcaret"></span>
    </a>
</li>