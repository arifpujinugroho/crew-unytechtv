<li class="{{ (Request::path() == 'home') ? 'active' : '' }}">
    <a href="{{ url ('/home')}}">
    <span class="pcoded-micon"><i class="ti-home"></i><b>H</b></span>
    <span class="pcoded-mtext" data-i18n="nav.dash.main">Home</span>
    <span class="pcoded-mcaret"></span>
    </a>
</li>
@if($pcb == 'true')
<li class="{{ (Request::path() == 'pcb-unytechtv') ? 'active' : '' }}">
    <a href="{{ url ('/pcb-unytechtv')}}">
        <span class="pcoded-micon"><i class="ti-pencil-alt"></i><b>PCB</b></span>
        <span class="pcoded-mtext" data-i18n="nav.form-pickers.main">Daftar Crew Baru {{ $tahun }}</span>
        <span class="pcoded-mcaret"></span>
    </a>
</li>
@else
<li class="{{ (Request::path() == 'pcb-unytechtv') ? 'active' : '' }}">
    <a href="{{ url ('/pcb-unytechtv')}}">
    <span class="pcoded-micon"><i class="ti-na"></i><b>PCB {{ $tahun }}</b></span>
    <span class="pcoded-mtext text-muted" data-i18n="nav.disabled-menu.main" >Daftar Crew Baru {{ $tahun }}</span>
        <span class="pcoded-mcaret"></span>
    </a>
</li>
@endif
<li class="{{ (Request::path() == 'struktur-organisasi') ? 'active' : '' }}">
    <a href="{{ url ('/struktur-organisasi')}}">
        <span class="pcoded-micon"><i class="ti-crown"></i><b>S O</b></span>
        <span class="pcoded-mtext" data-i18n="nav.advance-components.main">Pengurus Organisasi</span>
        <span class="pcoded-mcaret"></span>
    </a>
</li>
<li class="{{ (Request::path() == 'search-crew') ? 'active' : '' }}">
    <a href="{{ url ('/search-crew')}}">
        <span class="pcoded-micon"><i class="ti-search"></i><b>S</b></span>
        <span class="pcoded-mtext" data-i18n="nav.search.main">Temukan Crews</span>
        <span class="pcoded-mcaret"></span>
    </a>
</li>
<li class="">
    <a href="https://unytechtv.com/">
        <span class="pcoded-micon"><i class="ti-angle-double-left"></i><b>S</b></span>
        <span class="pcoded-mtext" data-i18n="nav.animations.main">Back Website Utama</span>
        <span class="pcoded-mcaret"></span>
    </a>
</li>