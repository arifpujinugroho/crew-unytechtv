<li class="{{ (Request::path() == 'mhs/home') ? 'active' : '' }}">
    <a href="{{ url ('/')}}">
    <span class="pcoded-micon"><i class="ti-home"></i><b>H</b></span>
    <span class="pcoded-mtext" data-i18n="nav.dash.main">Home</span>
    <span class="pcoded-mcaret"></span>
    </a>
</li>
<li class="{{ (Request::path() == 'mhs/aktivitas') ? 'active' : '' }}">
    <a href="{{ url ('mhs/aktivitas')}}">
        <span class="pcoded-micon"><i class="ti-layout-media-right"></i><b>T</b></span>
        <span class="pcoded-mtext" data-i18n="nav.invoice.main">Aktivitas</span>
        <span class="pcoded-mcaret"></span>
    </a>
</li>