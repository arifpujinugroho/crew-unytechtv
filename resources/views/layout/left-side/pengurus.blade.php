<li class="{{ (Request::path() == 'pengurus/home') ? 'active' : '' }}">
    <a href="{{ url ('/')}}">
    <span class="pcoded-micon"><i class="ti-home"></i><b>H</b></span>
    <span class="pcoded-mtext" data-i18n="nav.dash.main">Home</span>
    <span class="pcoded-mcaret"></span>
    </a>
</li>
<li class="{{ (Request::path() == 'pengurus/aktivitas') ? 'active' : '' }}">
    <a href="{{ url ('pengurus/aktivitas')}}">
        <span class="pcoded-micon"><i class="ti-layout-media-right"></i><b>T</b></span>
        <span class="pcoded-mtext" data-i18n="nav.invoice.main">Aktivitas</span>
        <span class="pcoded-mcaret"></span>
    </a>
</li>
<li class="{{ (Request::path() == 'pengurus/kegiatan') ? 'active' : '' }}">
    <a href="{{ url ('pengurus/kegiatan')}}">
        <span class="pcoded-micon"><i class="ti-layout-cta-right"></i><b>K U</b></span>
        <span class="pcoded-mtext" data-i18n="nav.navigate.main">Kegiatan UNYtechTV</span>
        <span class="pcoded-mcaret"></span>        
    </a>
</li>
<li class="{{ (Request::path() == 'pengurus/struktur') ? 'active' : '' }}">
    <a href="{{ url ('pengurus/struktur')}}">
        <span class="pcoded-micon"><i class="ti-crown"></i><b>S O</b></span>
        <span class="pcoded-mtext" data-i18n="nav.advance-components.main">Pengurus Organisasi</span>
        <span class="pcoded-mcaret"></span>
    </a>
</li>
<li class="{{ (Request::path() == 'pengurus/usercrew') ? 'active' : '' }}">
    <a href="{{ url ('pengurus/usercrew')}}">
        <span class="pcoded-micon"><i class="ti-user"></i><b>MC</b></span>
        <span class="pcoded-mtext" data-i18n="nav.user-profile.main">Management Crew</span>
        <span class="pcoded-mcaret"></span>
    </a>
</li>
<li class="{{ (Request::path() == 'pengurus/setting') ? 'active' : '' }}">
    <a href="{{ url ('pengurus/setting')}}">
        <span class="pcoded-micon"><i class="ti-settings"></i><b>M</b></span>
        <span class="pcoded-mtext" data-i18n="nav.maintenance.main">Setting</span>
        <span class="pcoded-mcaret"></span>
    </a>
</li>