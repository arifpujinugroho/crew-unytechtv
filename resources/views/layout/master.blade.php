<!DOCTYPE html>

<!--


	***** HAYO.. Mau Ngapain Buka Inpect Element? *****
   ******** JANGAN ISENG BUKA BUKA KODINGANNYA ***********

//=========================================================//
//         //\\       ||====    ||      ||==========       //
//        //  \\      ||  //    ||      ||                 //
//       //    \\     || //     ||      ||========         //
//      //======\\    || \\     ||      ||                 //
//     //        \\   ||  \\    ||      ||                 //
//    //          \\  ||   \\   ||      ||                 //
//                                                         //
//        Created By Arif Puji Nugroho (Indonesia)         //
//     Website    : https://arifpn.id/                     //
//     GitHub     : https://Github.com/arifpujinugroho     //
//     Facebook   : https://facebook.com/arifpujin         //
//     Instagram  : https://instagram.com/arifpn.id        //
//     Whatsapp   : +6285885994505                         //
//     Email      : arifpujinugroho@gmail.com              //
//=========================================================//

****Crew Management System for UKMF UNYtechTV FT UNY 2019****
****Pendamping Proyek Akhir Skripsi || Proyek Mandiri UNY****
         ***** 5 Maret 2019 - Thesis Finish *****





-->






<html lang="en">

<head>
    <title>@yield('title') || @guest UNYtechTV @else {{Auth::user()->level}} @endguest</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="author" content="Arif Puji Nugroho">

    {{--<!-- Favicon -->
    <link rel="icon" href="https://unytechtv.com/wp-content/uploads/2018/12/cropped-favicon-32x32.png" sizes="32x32" />
    <link rel="icon" href="https://unytechtv.com/wp-content/uploads/2018/12/cropped-favicon-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="https://unytechtv.com/wp-content/uploads/2018/12/cropped-favicon-180x180.png" />
    <meta name="msapplication-TileImage" content="https://unytechtv.com/wp-content/uploads/2018/12/cropped-favicon-270x270.png" />
    <script type='text/javascript' src='https://unytechtv.com/wp-content/themes/axen/layout/plugins/html5shiv/html5shiv.js?ver=5.0.4'></script>
    <script type='text/javascript' src='https://unytechtv.com/wp-content/themes/axen/layout/plugins/respond/respond.min.js?ver=5.0.4'></script>--}}

    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/bower_components/bootstrap/css/bootstrap.min.css')}}">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/icon/themify-icons/themify-icons.css')}}">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/icon/icofont/css/icofont.css')}}">
    <!-- flag icon framework css -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/pages/flag-icon/flag-icon.min.css')}}">
    <!-- Menu-Search css -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/pages/menu-search/css/component.css')}}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/style.css')}}">
    
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/jquery.mCustomScrollbar.css')}}">

    <!-- notify js Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/bower_components/pnotify/css/pnotify.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/bower_components/pnotify/css/pnotify.brighttheme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/bower_components/pnotify/css/pnotify.buttons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/bower_components/pnotify/css/pnotify.history.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/bower_components/pnotify/css/pnotify.mobile.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/pages/pnotify/notify.css')}}">

    <!-- sweet alert framework -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/bower_components/sweetalert/css/sweetalert.css')}}">
    
    <!-- animation nifty modal window effects css -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/component.css')}}">

    @yield('header')


</head>

<body>
    <!-- Pre-loader start -->
   <div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
        </div>
    </div>
</div>
    <!-- Pre-loader end -->

    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">

            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">

                    <div class="navbar-logo">
                        <a class="mobile-menu" id="mobile-collapse" href="#!">
                            <i class="ti-menu"></i>
                        </a>
                        @guest
                        <a href="https://unytechtv.com/">
                            <img class="img-fluid" src="{{url('assets/images/logo.png')}}" alt="Logo UNYtechTV" />
                        </a>
                        <a class="mobile-options" href="{{url('login')}}">
                            <i class="ti-key"></i><small> Login</small>
                        </a>
                        @else
                        <a href="#">
                            <img class="img-fluid" src="{{url('assets/images/logo.png')}}" alt="Logo UNYtechTV" />
                        </a>
                        <a class="mobile-options">
                            <i class="ti-more"></i>
                        </a>
                        @endguest
                    </div>

                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li>
                                <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
                            </li>
                            <li>
                                <a href="#!" onclick="javascript:toggleFullScreen()">
                                    <i class="ti-fullscreen"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                        @guest
                        <!--guest navbar-right-->
                            <li class="user-profile header-notification">
                                <a href="{{url('login')}}">
                                    <i class="ti-key"></i> Login
                                </a>
                            </li>
                        @else
                            <li class="user-profile header-notification">
                                <a href="#!">
                                    @if(Auth::user()->level == 'admin')
                                        <span>{{ Auth::user()->username }}</span>
                                    @elseif(Auth::user()->level == 'pengurus')
                                        <!--img src="" class="img-radius" alt="User-Profile-Image"-->
                                        <span>{{ Auth::user()->mahasiswa->namapanggilan }}</span>
                                        <i class="ti-angle-down"></i>
                                    @elseif(Auth::user()->level == 'mahasiswa')
                                        <!--img src="" class="img-radius" alt="User-Profile-Image"-->
                                        <span>{{ Auth::user()->mahasiswa->namapanggilan }}</span>
                                        <i class="ti-angle-down"></i>
                                    @endif
                                </a>
                                <ul class="show-notification profile-notification">
                                    <li style="cursor: pointer;" class="ganti-password">
                                        <i class="ti-lock"></i> Change Password
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}">
                                            <i class="ti-layout-sidebar-left"></i> Logout
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                        </ul>
                    </div>
                </div>
            </nav>


            <!-- Sidebar inner chat end-->
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <nav class="pcoded-navbar">
                        <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a>
                        </div>
                        
                        <div class="pcoded-inner-navbar main-menu">
                            <!--For Slide Samping-->
                            @guest
                             <!--guest navigation left-->
                            @else
                                @if(Auth::user()->level == 'mahasiswa' || Auth::user()->level == 'pengurus' || Auth::user()->level == 'preview')
                                    <div class="">
                                        <div class="main-menu-header">
                                            @if(Auth::user()->mahasiswa->fotoresmi == '' || Auth::user()->mahasiswa->fotoresmi == 'NULL')
                                                <img class="img-40 img-radius" src="{{url('storage/foto/'.Auth::user()->mahasiswa->tahunpcb.'/client/'.Auth::user()->mahasiswa->fotoawal)}}" alt="Foto {{Auth::user()->mahasiswa->namapanggilan}}">
                                            @else
                                                <img class="img-40 img-radius" src="{{url('storage/foto/'.Auth::user()->mahasiswa->tahunpcb.'/resmi/'.Auth::user()->mahasiswa->fotoresmi)}}" alt="Foto {{Auth::user()->mahasiswa->namapanggilan}}">
                                            @endif
                                            <div class="user-details">
                                                <span><strong>{{ Auth::user()->mahasiswa->namalengkap }}</strong></span>
                                                <span>{{ Auth::user()->mahasiswa->status_crew }} || {{ Auth::user()->username }}</span>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endguest

                            <div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation">Navigation
                            </div>

                            <ul class="pcoded-item pcoded-left-item">
                                @guest
                                    @include('layout.left-side.guest')
                                @else
                                    @if(Auth::user()->level == "admin")
                                        @include('layout.left-side.admin')
                                    @elseif(Auth::user()->level == "pengurus")
                                        @include('layout.left-side.pengurus')
                                    @elseif(Auth::user()->level == "mahasiswa")
                                        @include('layout.left-side.mahasiswa')
                                    @elseif(Auth::user()->level == "preview")
                                        @include('layout.left-side.preview')
                                    @endif
                                @endguest

                            </ul>
                        </div>
                    </nav>
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">
                                @yield('content')
                                </div>
                            </div>
                            <!-- Main-body end -->

                            {{--<div id="styleSelector">--}}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    @yield('end')

    @auth
    <div class="modal fade" id="ganti-Pass" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ganti Password</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                @if (Auth::user()->level == "admin")
                <form action="{{url('admin/gantipass')}}" class="form-validation" method="post">
                @elseif(Auth::user()->level == "pengurus")
                <form action="{{url('pengurus/gantipass')}}" class="form-validation" method="post">
                @elseif(Auth::user()->level == "preview")
                <form action="{{url('preview/gantipass')}}" class="form-validation" method="post">
                @else
                <form action="{{url('mhs/gantipass')}}" class="form-validation" method="post">
                @endif
                    @csrf                  
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                                <input type="password" name="password" id="password" minlength="6" class="validate[required] form-control" placeholder="Password">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="input-group">
                                    <span class="input-group-addon"><i class="icofont icofont-id-card"></i></span>
                                    <input type="password" name="password_ulang" class="validate[required,equals[password]] form-control" placeholder="Ulangi Password">
                            </div>
                        </div>
                    </div>           
                </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success waves-effect waves-light">Save Task</button>
                        <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
        
    @endauth

    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="{{url('assets/bower_components/jquery/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/bower_components/popper.js/js/popper.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="{{url('assets/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="{{url('assets/bower_components/modernizr/js/modernizr.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/bower_components/modernizr/js/css-scrollbars.js')}}"></script>

    <!-- i18next.min.js -->
    <script type="text/javascript" src="{{url('assets/bower_components/i18next/js/i18next.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/bower_components/jquery-i18next/js/jquery-i18next.min.js')}}"></script>

    <script src="{{url('assets/js/pcoded.min.js')}}"></script>
    
    <?php
    date_default_timezone_set("Asia/Jakarta");
    $b = time();
    $hour = date("G",$b);  
    ?>
    @if ($hour >= 17 ||  $hour <= 6 )
    <script src="{{url('assets/js/demo-dark.js')}}"></script>
    @else
    <script src="{{url('assets/js/demo-12.js')}}"></script> 
    @endif 

    <script src="{{url('assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/script.js')}}"></script>

    <!-- pnotify js -->
    <script type="text/javascript" src="{{url('assets/bower_components/pnotify/js/pnotify.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/bower_components/pnotify/js/pnotify.desktop.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/bower_components/pnotify/js/pnotify.buttons.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/bower_components/pnotify/js/pnotify.confirm.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/bower_components/pnotify/js/pnotify.callbacks.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/bower_components/pn otify/js/pnotify.animate.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/bower_components/pnotify/js/pnotify.history.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/bower_components/pnotify/js/pnotify.mobile.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/bower_components/pnotify/js/pnotify.nonblock.js')}}"></script>

    <!-- sweetalert -->
    <script type="text/javascript" src="{{url('assets/bower_components/sweetalert/js/sweetalert.min.js')}}"></script>
    <script type="text/javascript" src="assets/js/modal.js"></script>

    
    <!-- modalEffects js nifty modal window effects -->
    <script type="text/javascript" src="{{url('assets/js/modalEffects.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/classie.js')}}"></script>
    
    <script type="text/javascript" src="{{url('assets/js/jquery.validationEngine-en.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/js/jquery.validationEngine.js')}}"></script>

    @include('layout.browser')

    @yield('footer')

    <script>
        $(document).ready(function(){
            jQuery('.form-validation').validationEngine();
        });

        $('.ganti-password').click(function() {
            $('#ganti-Pass').modal('show');
        });
    </script>

    @include('layout.notification')   
</body>

</html>