<Script type="text/javascript">
    $(document).ready(function () {
//notify
    //guest
        @if (session('password') == "success")
            new PNotify({
                title: 'Berhasil',
                text: 'Akun, Anda sudah berhasil diganti',
                icon: 'icofont icofont-info-circle',
                type: 'success'
            });
        @endif
        @if (session('password') == "old")
            new PNotify({
                title: 'Failed',
                text: 'Password tidak boleh seperti sebelumnya!',
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        @endif
        @if (session('cache') == "clear")
            new PNotify({
                title: 'Great!',
                text: 'Cache and Session Clear',
                icon: 'icofont icofont-info-circle',
                type: 'success'
            });
        @endif
        @if (session('login') == "error")
            new PNotify({
                title: 'Gagal Login',
                text: 'Mohon cek kembali username dan Password anda atau Hubungi Admin',
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        @endif
        @if (session('login') == "denied")
            new PNotify({
                title: 'Access Denied!',
                text: 'Maaf anda harus login dahulu',
                icon: 'icofont icofont-info-circle',
                type: 'warning'
            });
        @endif
        @if (session('login') == "logout")
            new PNotify({
                title: 'Logout Successful!',
                text:  'Logout Success.',
                icon: 'icofont icofont-businessman',
                type: 'success'
            });
        @endif
        @if (session('login') == "notadmin")
            new PNotify({
                title: 'Access Denied!',
                text: 'Maaf status akses anda bukan sebagai Admin!',
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        @endif
        @if (session('login') == "notpengurus")
            new PNotify({
                title: 'Access Denied!',
                text: 'Maaf status akses anda bukan sebagai Pengurus!',
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        @endif
        @if (session('login') == "notmhs")
            new PNotify({
                title: 'Access Denied!',
                text: 'Maaf status akses anda bukan sebagai Mahasiswa!',
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        @endif
        @if (session('login') == "notpreview")
            new PNotify({
                title: 'Access Denied!',
                text: 'Maaf status akses anda bukan sebagai Preview!',
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        @endif
        @if (session('login') == "notlogin")
            new PNotify({
                title: 'Access Denied!',
                text: 'Maaf, anda belum login',
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        @endif
        @if (session('login') == "notguest")
            new PNotify({
                title: 'URL failed!',
                text: 'Maaf, anda sedang login',
                icon: 'icofont icofont-info-circle',
                type: 'error'
            });
        @endif
    //admin
        @if (session('adminaddtask') == "error")
                new PNotify({
                    title: 'Gagal',
                    text: 'Maaf, Nim Tidak ditemukan',
                    icon: 'icofont icofont-info-circle',
                    type: 'error'
                });
            @endif
            @if (session('adminaddtask') == "ada")
                new PNotify({
                    title: 'Failed',
                    text: 'Maaf, Nim sudah ditambah',
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            @endif
            @if (session('adminaddtask') == "success")
                new PNotify({
                    title: 'Berhasil',
                    text: 'Crew berhasil ditambahkan',
                    type: 'success'
                });
            @endif
            @if (session('adminaddtask') == "hapus")
                new PNotify({
                    title: 'Berhasil',
                    text: 'Crew berhasil dihapus',
                    type: 'success'
                });
            @endif
            @if (session('adminaddtask') == "avalible")
                new PNotify({
                    title: 'Gagal Simpan',
                    text: 'Maaf, Nim sudah terdaftar',
                    type: 'error'
                });
            @endif

    //Pengurus
            @if (session('tambahpengurus') == "success")
                new PNotify({
                    title: 'Berhasil',
                    text: 'Pengurus berhasil ditambah',
                    type: 'success'
                });
            @endif
            @if (session('tambahpengurus') == "ada")
                new PNotify({
                    title: 'Gagal',
                    text: 'Maaf, Pengurus sudah terdaftar, silakan hapus terlebih dahulu',
                    type: 'error'
                });
            @endif
            @if (session('hapuspengurus') == "success")
                new PNotify({
                    title: 'Berhasil',
                    text: 'Pengurus berhasil dihapus',
                    type: 'success'
                });
            @endif
            @if (session('penguruskelola') == "ada")
                new PNotify({
                    title: 'Failed',
                    text: 'Maaf, Nim sudah sebagai preview',
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            @endif
            @if (session('penguruskelola') == "pengurus")
                new PNotify({
                    title: 'Failed',
                    text: 'Maaf, Nim sudah sebagai Pengurus',
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            @endif
            @if (session('penguruskelola') == "over")
                new PNotify({
                    title: 'Failed',
                    text: 'Maaf, Kuota pengelola sudah penuh',
                    icon: 'icofont icofont-info-circle',
                    type: 'warning'
                });
            @endif
            @if (session('penguruskelola') == "success")
                new PNotify({
                    title: 'Berhasil',
                    text: 'NIM berhasil ditambahkan',
                    type: 'success'
                });
            @endif
            @if (session('penguruskelola') == "hapus")
                new PNotify({
                    title: 'Berhasil',
                    text: 'Akses Nim berhasil dihapus',
                    type: 'success'
                });
            @endif
            @if (session('pengurusresmi') == "kosong")
                new PNotify({
                    title: 'Berhasil',
                    text: 'Foto resmi berhasil diupload',
                    type: 'success'
                });
            @endif
            @if (session('pengurusresmi') == "ada")
                new PNotify({
                    title: 'Berhasil',
                    text: 'Foto resmi berhasil diupdate',
                    type: 'success'
                });
            @endif

//sweetalert
        @if (session('register') == "success")
                swal("Success!", "Silakan tunggu Pengumuman selanjutnya :)", "success");
        @endif
        @if (session('register') == "updated")
                swal("Updated Data!", "Data sudah di update :)", "success");
        @endif
        @if (session('register') == "registered")
            swal("User Registered", "Mahasiswa telah terdaftar", "error");
        @endif
    });
</Script>