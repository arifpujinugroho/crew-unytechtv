<?php
use App\AddConfig;
use Illuminate\Support\Facades\Artisan;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|Route::get('/', function () {
|    return view('welcome');
|});
*/

Route::get('/crew/{nim}', 'GetGuestController@Nim');
Route::get('/logout', function()
{
	Auth::logout();
    return Redirect::to('/login')
    ->with('login', 'logout');
})->name('logout');

Route::group(['middleware' => ['lgn']], function () {
    Route::get('/admin', function () {
        return Redirect::to('/admin/home');
    });
    Route::get('/pengurus', function () {
        return Redirect::to('/pengurus/home');
    });
    Route::get('/preview', function () {
        return Redirect::to('/preview/home');
    });
    Route::get('/mhs', function () {
        return Redirect::to('/mhs/home');
    });
});

Route::get('/', function () {
    if(Auth::check())
    {
        $leveluser = Auth::user()->level;
        $maintenance = AddConfig::wherename('modemaintenance')->first()->isinya;

        if ($leveluser == 'admin'){
            return Redirect::to('/admin');
        } elseif ($leveluser == 'pengurus'){
            return Redirect::to('/pengurus');
        } elseif ($leveluser == 'mahasiswa'){
            if($maintenance == 'true'){
                return Redirect::to('/logout');
            } else {
                return Redirect::to('/mhs');
            }
        }elseif ($leveluser == 'preview'){
            if($maintenance == 'true'){
                return Redirect::to('/logout');
            } else {
                return Redirect::to('/preview');
            }
        }
    } else {
        return Redirect::to('/home');
    }
});


//Guest
Route::group(['middleware' => ['guest']], function () {
    Route::get('/home', 'GetGuestController@Index');
    Route::get('/login', 'GetGuestController@Login');
    Route::get('/pcb-unytechtv', 'GetGuestController@Register');
    Route::get('/struktur-organisasi', 'GetGuestController@Pengurus');
    Route::get('/search-crew', 'GetGuestController@SearchCrew');
    route::get('/search', 'GetGuestController@Search');
    route::get('/crew{thn}', 'GetGuestController@Custom');
    Route::get('cache', function () {
       $viewCode  = Artisan::call('view:clear');
       $cacheCode = Artisan::call('cache:clear');
       $routeCode = Artisan::call('route:clear');
      return Redirect()->back()->with('cache','clear');
    });
    
    Route::post('/login','PostGuestController@Login')->name('login');
    route::post('/pcb-unytechtv', 'PostGuestController@Daftar');
    route::post('/search', 'PostGuestController@Search');
    route::post('/crew{thn}', 'PostGuestController@Custom');
});



//Admin
Route::prefix('admin')->group(function () {
//Get
    Route::get('home','GetAdminController@Index');
    Route::get('search-crew', 'GetAdminController@SearchCrew');
    route::get('search', 'GetAdminController@Search');
    Route::get('kegiatan','GetAdminController@Kegiatan');
    Route::get('kegiatan/{id}','GetAdminController@AddTaskCrew');
    Route::get('kegiatan/{id}/print','GetAdminController@Print');
    Route::get('pengelola','GetAdminController@Pengelola');

//Post
    route::post('gantipass', 'PostAdminController@GantiPass');
    route::post('search', 'PostAdminController@Search');
    route::post('addkegiatan', 'PostAdminController@AddKegiatan');
    route::post('addcrew','PostAdminController@AddCrew');
    route::post('delcrew','PostAdminController@DelCrew');
    route::post('addpengelola','PostAdminController@AddPengelola');
    route::post('delpengelola','PostAdminController@DelPengelola');

});


Route::prefix('pengurus')->group(function () {
    //Get
        Route::get('home','GetPengurusController@Index');
        Route::get('aktivitas','GetPengurusController@Activity');
        Route::get('struktur','GetPengurusController@Struktur');
        Route::get('usercrew','GetPengurusController@UserCrew');
        Route::get('pengelola','GetPengurusController@KelolaWeb');
        Route::get('kegiatan','GetPengurusController@Kegiatan');
        Route::get('kegiatan/{id}','GetPengurusController@AddTaskCrew');
        Route::get('caloncrew','GetPengurusController@CalonCrew');
        Route::get('pcb-up', 'GetPengurusController@PCBUP');
        Route::get('pcb-down', 'GetPengurusController@PCBDOWN');
        Route::get('setting', 'GetPengurusController@Setting');
        Route::get('user', 'GetPengurusController@User');

        
    
    //Post
        route::post('gantipass', 'PostPengurusController@GantiPass');
        route::post('tambahpengurus', 'PostPengurusController@TambahPengurus');
        route::post('hapuspengurus', 'PostPengurusController@HapusPengurus');
        route::post('addpengelola','PostPengurusController@AddPengelola');
        route::post('addpreview','PostPengurusController@AddPreview');
        route::post('delpengelola','PostPengurusController@DelPengelola');
        route::post('fotoresmi','PostPengurusController@FotoResmi');
        route::post('gantifoto','PostPengurusController@GantiFoto');
        route::post('reset-pass','PostPengurusController@ResetPass');
    });


    Route::prefix('mhs')->group(function () {
    //Get
        Route::get('home','GetMahasiswaController@Index');
        Route::get('aktivitas','GetMahasiswaController@Activity');
        
    //Post
        route::post('gantipass', 'PostMahasiswaController@GantiPass');
        route::post('gantifoto','PostMahasiswaController@GantiFoto');
    });


/*fakeget
Route::get('/dpo2019', 'GetGuestController@Dpo2019');
Route::get('/pengurus2019', 'GetGuestController@Pengurus2019');
Route::get('/crew2018', 'GetGuestController@Crew2018');

route::post('/dpo2019', 'PostGuestController@Dpo2019');
route::post('/pengurus2019', 'PostGuestController@Pengurus2019');
route::post('/crew2018', 'PostGuestController@Crew2018');
*/